//
//  SignupVC.swift
//  Abeer Food Rider
//
//  Created by USER on 09/10/20.
//  Copyright © 2020 Cloudwapp. All rights reserved.
//

import UIKit
import DropDown
import SkyFloatingLabelTextField
import NVActivityIndicatorView

@available(iOS 13.0, *)
@available(iOS 14.0, *)
class SignupVC: UIViewController {
    
    // MARK: - IBDECS:

    var checkTag = 0
    
    // MARK: - IBOUTLETS:
    
    @IBOutlet weak var scrollVw: UIScrollView!

     @IBOutlet weak var emailTxt: SkyFloatingLabelTextField!
    @IBOutlet weak var pwTxt: SkyFloatingLabelTextField!
   @IBOutlet weak var fnameTxt: SkyFloatingLabelTextField!
    @IBOutlet weak var lnameTxt: SkyFloatingLabelTextField!
    @IBOutlet weak var phoneTxt: SkyFloatingLabelTextField!
     @IBOutlet weak var socialSecTxt: SkyFloatingLabelTextField!
    @IBOutlet weak var iownTxt: SkyFloatingLabelTextField!
     @IBOutlet weak var availableTxt: SkyFloatingLabelTextField!
     @IBOutlet weak var mysmartPhoneTxt: SkyFloatingLabelTextField!
    @IBOutlet weak var checkBtn: UIButton!
   

    // MARK: - IBActions
     
    
    @IBAction func iownAction(_ sender: Any) {
        self.owndropDown.show()


    }
 @IBAction func availableAction(_ sender: Any) {
    self.availabledropDown.show()

    }
    @IBAction func smartPhoneAction(_ sender: Any) {
        self.smartphonedropDown.show()

    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
     }
    
      @IBAction func signupBtnAction(_ sender: Any) {
       
        
//        @IBOutlet weak var fnameTxt: SkyFloatingLabelTextField!
//        @IBOutlet weak var lnameTxt: SkyFloatingLabelTextField!
//        @IBOutlet weak var phoneTxt: SkyFloatingLabelTextField!
//         @IBOutlet weak var socialSecTxt: SkyFloatingLabelTextField!
//        @IBOutlet weak var iownTxt: SkyFloatingLabelTextField!
//         @IBOutlet weak var availableTxt: SkyFloatingLabelTextField!
//         @IBOutlet weak var mysmartPhoneTxt: SkyFloatingLabelTextField!
        
        
        if fnameTxt.text!.isEmpty {
                 self.view.makeToast("First Name required!!!")
                      }
       else if lnameTxt.text!.isEmpty {
                        self.view.makeToast("Last Name required!!!")
                             }
            else if emailTxt.text!.isEmpty {
                       
              self.view.makeToast("Email required!!!")

                   }
              else  if !((emailTxt.text?.isValidEmail())!) {
                  self.view.makeToast("Valid Email required!!!")

              }
            else  if pwTxt.text!.isEmpty {
              
               self.view.makeToast("Password required!!!")

                          }
//            else if phoneTxt.text!.isEmpty {
//                                   self.view.makeToast("Phone Number required!!!")
//                                        }
//        else if socialSecTxt.text!.isEmpty {
//            
//            self.view.makeToast("Social Security Number required!!!")
//              }
            else if iownTxt.text!.isEmpty {
                       
                       self.view.makeToast("what you own is required!!!")
                         }
            else if availableTxt.text!.isEmpty {
                       
                       self.view.makeToast("When yopu will be available is required!!!")
                         }
            else if checkBtn.isSelected == false
            {
        self.view.makeToast("Please check Terms and conditions!!!")

            }
//            else if mysmartPhoneTxt.text!.isEmpty {
//
//                                  self.view.makeToast("Smartphone type is required!!!")
//                                    }
            else
        {
            
            SignupCalling()
            
        }
    
        
    }
  
    @available(iOS 14.0, *)
    @IBAction func termsBtnAction(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AboutUsVC") as! AboutUsVC
           self.navigationController?.pushViewController(vc, animated: true)
       }
    
    @IBAction func checkBtnAction(_ sender: Any) {
        
        if checkTag == 0
        {
            checkBtn.isSelected = true
            checkTag = 1
        }
        else {
            checkBtn.isSelected = false
            checkTag = 0
        }
        
        
          }
    @IBAction func loginBtnAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "loginVC") as! loginVC
                  self.navigationController?.pushViewController(vc, animated: true)
        
             }
    
    let owndropDown = DropDown()
    let availabledropDown = DropDown()
    let smartphonedropDown = DropDown()

    // The view to which the drop down will appear on
 

    override func viewDidLoad() {
        super.viewDidLoad()

        // for i own tf
           owndropDown.anchorView = iownTxt
         owndropDown.dataSource = ["Car", "Scooter", "Bike"]
          owndropDown.selectionAction = { [unowned self] (index: Int, item: String) in

            self.iownTxt.text = "\(item)"
            self.owndropDown.hide()
        
        }
    
        // for available tf

    availabledropDown.anchorView = availableTxt
     availabledropDown.dataSource = ["Weekdays", "Weekends", "Both"]
       availabledropDown.selectionAction = { [unowned self] (index: Int, item: String) in

        self.availableTxt.text = "\(item)"
        self.availabledropDown.hide()
     
     }
    
    // for smartphone tf

    smartphonedropDown.anchorView = mysmartPhoneTxt
     smartphonedropDown.dataSource = ["IOS", "Android"]
       smartphonedropDown.selectionAction = { [unowned self] (index: Int, item: String) in

        self.mysmartPhoneTxt.text = "\(item)"
        self.smartphonedropDown.hide()
     
     }

        
    forStsBar()

          
           }
           func forStsBar()
               {
                   if #available(iOS 13.0, *) {
                       let app = UIApplication.shared
                       let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                       
                       let statusbarView = UIView()
                       statusbarView.backgroundColor = themeColorBrown
                       view.addSubview(statusbarView)
                     
                       statusbarView.translatesAutoresizingMaskIntoConstraints = false
                       statusbarView.heightAnchor
                           .constraint(equalToConstant: statusBarHeight).isActive = true
                       statusbarView.widthAnchor
                           .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                       statusbarView.topAnchor
                           .constraint(equalTo: view.topAnchor).isActive = true
                       statusbarView.centerXAnchor
                           .constraint(equalTo: view.centerXAnchor).isActive = true
                     
                   } else {
                       let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                       statusBar?.backgroundColor = themeColorBrown
                   }
         }
    
    override func viewDidLayoutSubviews() {
           
           self.scrollVw.contentSize.height =  1200
        
     
    }
    
    // MARK: - WebService Calling

           func SignupCalling()  {
               let type = NVActivityIndicatorType.ballScaleRippleMultiple
               let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
               let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColor, padding: 20)
               self.view.addSubview(activityIndicatorView)
               self.view.isUserInteractionEnabled = false
              


               activityIndicatorView.startAnimating()
               var param = [String: Any]()
            let fcmToken = UserDefaults.standard.value(forKey: "self.fcmToken") as! String

               param["fname"] = self.fnameTxt.text!
               param["lname"] = self.lnameTxt.text!
            param["email"] = self.emailTxt.text!
            param["phone"] = self.phoneTxt.text!
            param["password"] = self.pwTxt.text!
            param["vehicle"] = self.iownTxt.text!
            param["work_on"] = self.availableTxt.text!
            param["smartphone"] = self.mysmartPhoneTxt.text!
            param["security_no"] = self.socialSecTxt.text!
            param["fcm_token"] = fcmToken

               WebService().postRequest(methodName: register, parameter: param) { (result) in
       activityIndicatorView.stopAnimating()
                   self.view.isUserInteractionEnabled = true

                   if let resultDict = result as? NSDictionary {
                       if resultDict.value(forKey: "status") as! Bool == true {
                      
                                 if let userData = resultDict.value(forKey: "data") as? NSDictionary {
                                                
//                                                UserDefaultManager().setUserDefault(value: userData, key: "userData")
//
//                                                self.view.makeToast("Logged in Successfully!!")

                                                
                                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "loginVC") as! loginVC
                                            self.navigationController?.pushViewController(vc, animated: true)
                                    
                                            }
                                        }
                                        else
                                        {
                                    
                             self.view.makeToast(resultDict.value(forKey: "message") as! String)
                                            
//                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "loginVC") as! loginVC
//                           self.navigationController?.pushViewController(vc, animated: true)
                                      // self.view.makeToast("Driver not registered")

                                        }
                                        
                                        
                                        
                                    }
                                }
                         

                        }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
