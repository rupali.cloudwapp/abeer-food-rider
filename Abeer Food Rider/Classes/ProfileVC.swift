//
//  ProfileVC.swift
//  Gofa Rider
//
//  Created by USER on 02/10/20.
//  Copyright © 2020 Cloudwapp. All rights reserved.
//

import UIKit
import AlamofireImage
import NVActivityIndicatorView
import Toast_Swift


@available(iOS 13.0, *)
class ProfileVC: UIViewController {
    
    // MARK: - IBOUTLETS
       
    @IBOutlet weak var profile_img: UIImageView!
    @IBOutlet weak var NmLbl: UILabel!
    @IBOutlet weak var NumberLbl: UILabel!
    @IBOutlet weak var EmailLbl: UILabel!
    @IBOutlet weak var SCROLL_VIEW: UIScrollView!

       
       // MARK: - IBActions
       
    
    @IBAction func profileAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "myprofileVC") as! myprofileVC
               self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func bankdetailAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BankDetailVC") as! BankDetailVC
                  self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func chngpw_action(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChangePwVC") as! ChangePwVC
                      self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func logoutAction(_ sender: Any) {
        
        confiremLogout()
        
    }
    
    
    
    
    @IBAction func earningAction(_ sender: Any) {
           
           let vc = self.storyboard?.instantiateViewController(withIdentifier: "EarningVC") as! EarningVC
           self.navigationController?.pushViewController(vc, animated: true)
           
       }

    override func viewDidLoad() {
        super.viewDidLoad()

             forStsBar()
        
          }
    override func viewWillAppear(_ animated: Bool) {
                SCROLL_VIEW.contentSize.height = 550

    }
    
    override func viewDidLayoutSubviews() {
        SCROLL_VIEW.contentSize.height = 550
    }
   
    
    
          func forStsBar()
              {
                  if #available(iOS 13.0, *) {
                      let app = UIApplication.shared
                      let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                      
                      let statusbarView = UIView()
                      statusbarView.backgroundColor = themeColorBrown
                      view.addSubview(statusbarView)
                    
                      statusbarView.translatesAutoresizingMaskIntoConstraints = false
                      statusbarView.heightAnchor
                          .constraint(equalToConstant: statusBarHeight).isActive = true
                      statusbarView.widthAnchor
                          .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                      statusbarView.topAnchor
                          .constraint(equalTo: view.topAnchor).isActive = true
                      statusbarView.centerXAnchor
                          .constraint(equalTo: view.centerXAnchor).isActive = true
                    
                  } else {
                      let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                      statusBar?.backgroundColor = themeColorBrown
                  }
        }
    func confiremLogout() {
        let alert = UIAlertController(title: "Alert", message: "Are you sure, You want logout?", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Confirm", style: .default) { (action) in
            
            self.Logout()
          
        }
        let noAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alert.addAction(okAction)
        alert.addAction(noAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func Logout()  {
               let type = NVActivityIndicatorType.ballScaleRippleMultiple
               let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
               let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColor, padding: 20)
               self.view.addSubview(activityIndicatorView)
               self.view.isUserInteractionEnabled = false

               activityIndicatorView.startAnimating()
               var param = [String: Any]()
           let userData = UserDefaultManager().getUserDefault(key: "userData") as? NSDictionary
           
           param["driver_id"] = userData?.value(forKey: "id") as! String

               WebService().postRequest(methodName: logout, parameter: param) { (result) in
       activityIndicatorView.stopAnimating()
                   self.view.isUserInteractionEnabled = true

                   if let resultDict = result as? NSDictionary {
                       
                       print("resultDict",resultDict)
                       if resultDict.value(forKey: "status") as! Bool == true {
                                 if #available(iOS 13.0, *) {
                                                                                                                        
                                                             let scene = UIApplication.shared.connectedScenes.first
                                                        if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                                                           
                                                      sd.setLoginrPage()
                                                           
                                                        }
                                                   }
                                                   else  {
                                appDele.setLoginrPage()
                                                       }
                        
                        
                        UserDefaultManager().setUserDefault(value: "" as AnyObject, key: "userData")
                          
                           }
                       else
                       {
                      self.view.makeToast("Sorry,Not able to logout.")
                       }
                   }
               }
       }
    
 
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
