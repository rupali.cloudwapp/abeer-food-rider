//
//  OrdersDetailVC.swift
//  Gofa Rider
//
//  Created by USER on 04/10/20.
//  Copyright © 2020 Cloudwapp. All rights reserved.
//

import UIKit
import AlamofireImage
import NVActivityIndicatorView

class OrdersDetailVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
 
    // MARK: - IBDeclarations :

    var dict = NSDictionary()
    var storeDict = NSDictionary()
    var orderDict = NSDictionary()

    var itemArray = NSArray()
    var action = String()
    var adonArray = NSArray()
    
    var phoneArr = NSArray()

    // MARK: - IBOutlets :
    
    @IBOutlet weak var calltbl_height: NSLayoutConstraint!
    @IBOutlet weak var callllist_table: UITableView!
    @IBOutlet weak var callList_popup: UIView!
    
    @IBOutlet weak var insidescroll_height: NSLayoutConstraint!
    @IBOutlet weak var scrollVw: UIScrollView!
    @IBOutlet weak var itemTblView: UITableView!
    @IBOutlet weak var itemTblHeight: NSLayoutConstraint!
    @IBOutlet weak var subTotalLbl: UILabel!
    @IBOutlet weak var deliverychrgsLbl: UILabel!
    @IBOutlet weak var taxesLbl: UILabel!
    @IBOutlet weak var payByLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var clickHereBtn: UIButton!
    @IBOutlet weak var bottomImgVw: UIImageView!
    @IBOutlet weak var BottomHeadingLbl: UILabel!
    @IBOutlet weak var bottomDesclBL: UILabel!
    @IBOutlet weak var TopheadingLbl: UILabel!
    
    @IBOutlet weak var topDescLbl: UILabel!
    @IBOutlet weak var customerNote_lbl: UILabel!
    
    @IBOutlet weak var sidePopup: UIView!
    @IBOutlet weak var pickupLocLbl: UILabel!
    
    @IBOutlet weak var DroplocLbl: UILabel!
    
    @IBOutlet weak var pickupHeight: NSLayoutConstraint!
    @IBOutlet weak var dropHeight: NSLayoutConstraint!
    
    @IBOutlet weak var totalHeight: NSLayoutConstraint!
   
    
    @IBOutlet weak var vendornote_height: NSLayoutConstraint!
    @IBOutlet weak var vendornote_lbl: UILabel!
    @IBOutlet weak var summaryview_height: NSLayoutConstraint!
    @IBOutlet weak var bottomvw_height: NSLayoutConstraint!
    @IBOutlet weak var customerNoteHeight: NSLayoutConstraint!
    @IBOutlet weak var customernote_vw: UIView!
//    @IBOutlet weak var clickheretop: NSLayoutConstraint!
    // MARK: - IBActions :
    
    @IBAction func calllistResign_act(_ sender: Any) {
        
        self.callList_popup.isHidden = true
        
    }
    @IBAction func backAction(_ sender: Any) {
      
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func clickHereAction(_ sender: Any) {
        var Message = String()
        if self.clickHereBtn.titleLabel?.text == "Click here to Start delivery"
                          {
                         Message = "You want to Start delivery?"
                          }
                   else if self.clickHereBtn.titleLabel?.text ==  "Click here to Finish delivery"
                          {
                              Message = "You want to finish delivery?"

                          }
        
        // Create the alert controller
           let alertController = UIAlertController(title: "Are you sure", message: Message, preferredStyle: .alert)

               // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                   UIAlertAction in
                
            if self.clickHereBtn.titleLabel?.text == "Click here to Start delivery"
                   {
                    self.deliveryStart()
                   }
            else if self.clickHereBtn.titleLabel?.text ==  "Click here to Finish delivery"
                   {
                       self.deliveryEnd()

                   }
                   else
                   {
                       
                   }
                
                
               }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
                   UIAlertAction in
               }

               // Add the actions
               alertController.addAction(okAction)
               alertController.addAction(cancelAction)

               // Present the controller
               self.present(alertController, animated: true, completion: nil)
        
         
    }
    
    @IBAction func closeAction(_ sender: Any) {
        
        UIView.transition(with: self.sidePopup,
                                       duration: 0.1,
                                    options: [.transitionCrossDissolve],
                                    animations: {
                                self.sidePopup.isHidden = true
                                                        },
                             completion: nil)
        
    }
    @IBAction func navigateAction(_ sender: Any) {
        
        let storeDict = dict.value(forKey: "storedetail") as! NSDictionary

        let latStr = storeDict.value(forKey: "lat") as! String
        let longStr = storeDict.value(forKey: "lon") as! String

        
        if latStr == ""
            {
                self.view.makeToast("latitude and longitude not found")
            }
            else if longStr == ""
            {
                self.view.makeToast("latitude and longitude not found")

            }
            else
                
            {
                if let urlDestination = URL.init(string: "https://www.google.co.in/maps/dir/?saddr=&daddr=\(latStr),\(longStr)&directionsmode=driving") {
                    UIApplication.shared.openURL(urlDestination)
                }
                    
                else
                    
                    {
                self.view.makeToast("Can't use comgooglemaps://")
                    NSLog("Can't use comgooglemaps://");
                    }
                }
    }
    
    @IBAction func callAction(_ sender: Any) {
        
        if clickHereBtn.titleLabel!.text == "Click here to Start delivery"
        {
            
        ContactList()
            
        }
        else
        {
        
        let storeDict = dict.value(forKey: "storedetail") as! NSDictionary
        let number = storeDict.value(forKey: "phone") as! String
        if let url = URL(string: "tel://\(number)"),
                  UIApplication.shared.canOpenURL(url) {
                  if #available(iOS 10, *) {
                      UIApplication.shared.open(url, options: [:], completionHandler:nil)
                  } else {
                      UIApplication.shared.openURL(url)
                  }
              } else {
            self.view.makeToast("Call error")
              }
        }
        
        
            
    }
    
    
    @IBAction func menuAction(_ sender: Any) {
        
        UIView.transition(with: self.sidePopup,
                                  duration: 0.1,
                               options: [.transitionCrossDissolve],
                               animations: {
                           self.sidePopup.isHidden = false
                                                   },
                        completion: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print("dict",dict)
        
        scrollVw.flashScrollIndicators()
        
        self.TopheadingLbl.isHidden = false
        self.topDescLbl.isHidden = false


        
        self.sidePopup.isHidden = true
        
        self.storeDict = dict.value(forKey: "storedetail") as! NSDictionary
        self.orderDict = dict.value(forKey: "orderdata") as! NSDictionary
        self.itemArray = dict.value(forKey: "menudetail") as! NSArray
        
        let vennote = self.orderDict.value(forKey: "note") as! String
        let cusnote = self.orderDict.value(forKey: "d_note") as! String

        self.vendornote_lbl.text =    "Vendor Note :-" + vennote
        self.customerNote_lbl.text = "Customer Note :-" + cusnote

        if self.orderDict.value(forKey: "d_note") as! String == ""
        {
            customernote_vw.isHidden = true
         customerNoteHeight.constant = 0
        }
        else
        {
            customernote_vw.isHidden = false
           customerNoteHeight.constant = 60
        }
        
       if self.orderDict.value(forKey: "note") as! String == ""
        {
            self.vendornote_lbl.isHidden = true
            self.vendornote_height.constant = 0
            
        }
        else
        {
            
         self.vendornote_lbl.isHidden = false
            self.vendornote_height.constant = 60

        }
        
        

        
//        itemTblHeight.constant = CGFloat(itemArray.count * 60)
//                if itemArray.count > 4 {
//                   itemTblHeight.constant = 240.0
//                }
        
        
        
        
        
        
        itemTblView.delegate = self
        itemTblView.dataSource = self
        itemTblView.reloadData()
        
            subTotalLbl.text = "kr \(orderDict.value(forKey: "total_amount") as! String)"
             deliverychrgsLbl.text = "kr \(orderDict.value(forKey: "delivery_charges") as! String)"
        
        
        let payment_status = orderDict.value(forKey: "payment_status") as! String
        if payment_status == "Complete"
        {
          payByLbl.text = "Already paid"
        }
        else
        {
        payByLbl.text = "Pay by " + "\(orderDict.value(forKey: "payment_method") as! String)"
        }
             
             let total = Double(orderDict.value(forKey: "total_amount") as! String)!
             let deliveryCahrges = Double(orderDict.value(forKey: "delivery_charges") as! String)!
             var totalPrice = 0.0
             totalPrice = total + deliveryCahrges
             let tax = Double(orderDict.value(forKey: "tax_amount") as! String)!
             let newwtaxcome = Double(round(100*tax)/100)
          self.taxesLbl.text = "kr \(newwtaxcome)"
         let newwtotalItemPrice = Double(round(100*totalPrice)/100) + newwtaxcome
             totalLbl.text =  "kr \(newwtotalItemPrice)"
        let imgShow = storeDict.value(forKey: "image") as! String
            self.bottomImgVw.af_setImage(withURL: URL(string: "\(img_Url)\(imgShow)")!)


       managingButtonTitlebyAction()
        
        pickupLocLbl.text = storeDict.value(forKey: "address") as! String
        DroplocLbl.text = orderDict.value(forKey: "user_address") as! String

        pickupHeight.constant = CGFloat(20*pickupLocLbl.calculateMaxLines() + 80)
        
        print("pickupLocLbl.calculateMaxLines()",pickupLocLbl.calculateMaxLines())
        print("DroplocLbl.calculateMaxLines()",DroplocLbl.calculateMaxLines())

        
        
        dropHeight.constant = CGFloat(20*DroplocLbl.calculateMaxLines() + 80)
        totalHeight.constant = pickupHeight.constant + dropHeight.constant
        + 20
         print("pickupHeight",pickupHeight.constant)
        print("dropHeight",dropHeight.constant)
        print("totalHeight",totalHeight.constant)

        
    forStsBar()
        
    }
    
    func forStsBar()
                {
                    if #available(iOS 13.0, *) {
                        let app = UIApplication.shared
                        let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                        
                        let statusbarView = UIView()
                        statusbarView.backgroundColor = themeColorBrown
                        view.addSubview(statusbarView)
                      
                        statusbarView.translatesAutoresizingMaskIntoConstraints = false
                        statusbarView.heightAnchor
                            .constraint(equalToConstant: statusBarHeight).isActive = true
                        statusbarView.widthAnchor
                            .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                        statusbarView.topAnchor
                            .constraint(equalTo: view.topAnchor).isActive = true
                        statusbarView.centerXAnchor
                            .constraint(equalTo: view.centerXAnchor).isActive = true
                      
                    } else {
                        let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                        statusBar?.backgroundColor = themeColorBrown
                    }
          }
    func managingButtonTitlebyAction()
    {
        if action == "new"
               {
                   
            TopheadingLbl.text = "Pickup Location"
                   BottomHeadingLbl.text = "Pickup from"
                   topDescLbl.text = storeDict.value(forKey: "address") as! String
                   let name = storeDict.value(forKey: "name") as! String
                   let address = storeDict.value(forKey: "address") as! String
               bottomDesclBL.text = name + "\n" + address
                   clickHereBtn.setTitle("Click here to Start delivery", for: .normal)
                   clickHereBtn.isUserInteractionEnabled = true
                
                if self.orderDict.value(forKey: "d_note") as! String == ""
                      {
                          customernote_vw.isHidden = true
                       customerNoteHeight.constant = 0
                      }
                      else
                      {
                          customernote_vw.isHidden = false
                         customerNoteHeight.constant = 60
                      }
                      
                     if self.orderDict.value(forKey: "note") as! String == ""
                      {
                          self.vendornote_lbl.isHidden = true
                          self.vendornote_height.constant = 0
                          
                      }
                      else
                      {
                          
                       self.vendornote_lbl.isHidden = false
                          self.vendornote_height.constant = 60

                      }
                
                
                


               }
               else if action == "current"
               {
                  TopheadingLbl.text = "Drop Location"
                   BottomHeadingLbl.text = "Drop Location"
               topDescLbl.text = orderDict.value(forKey: "user_address") as! String
                                    let name = orderDict.value(forKey: "username") as! String
                                    let address = orderDict.value(forKey: "user_address") as! String
                                bottomDesclBL.text = name + "\n" + address
                   clickHereBtn.setTitle("Click here to Finish delivery", for: .normal)
                   clickHereBtn.isUserInteractionEnabled = true
                
                customernote_vw.isHidden = true
                                      customerNoteHeight.constant = 0
                self.vendornote_lbl.isHidden = true
                self.vendornote_height.constant = 0
                
               }
               else
               {
                 TopheadingLbl.text = "Pickup Location"
                             BottomHeadingLbl.text = "Pickup from"
                  topDescLbl.text = storeDict.value(forKey: "address") as! String
                            let name = storeDict.value(forKey: "name") as! String
                            let address = storeDict.value(forKey: "address") as! String
                        bottomDesclBL.text = name + "\n" + address
                   clickHereBtn.setTitle("Order Completed", for: .normal)
                   clickHereBtn.isUserInteractionEnabled = false
                
                customernote_vw.isHidden = true
               customerNoteHeight.constant = 0
                self.vendornote_lbl.isHidden = true
                self.vendornote_height.constant = 0

               }
        
        itemTblHeight.constant = tableViewHeight
             summaryview_height.constant = tableViewHeight + vendornote_height.constant + 180
             self.scrollVw.contentSize.height = summaryview_height.constant + 40
             print("self.scrollVw.contentSize.height",self.scrollVw.contentSize.height)
             self.view.layoutIfNeeded()
        
        
        
    }
    override func viewDidLayoutSubviews() {
        
      //  self.scrollVw.contentSize.height = self.tableViewHeight + 450
        
      //  self.scrollVw.contentSize.height = summaryview_height.constant + 200

      //  self.scrollVw.contentSize.height = summaryview_height.constant + 200

    // self.scrollVw.contentSize.height = 800
        itemTblHeight.constant = tableViewHeight
    summaryview_height.constant = tableViewHeight + vendornote_height.constant + 180
self.scrollVw.contentSize.height = summaryview_height.constant + 40
        print("self.scrollVw.contentSize.heightdidlayout",self.scrollVw.contentSize.height)
         //   insidescroll_height.constant = summaryview_height.constant + 200
            self.view.layoutIfNeeded()
        
    }
    override func viewWillLayoutSubviews() {
            itemTblHeight.constant = tableViewHeight
            summaryview_height.constant = tableViewHeight + vendornote_height.constant + 180
            
            self.scrollVw.contentSize.height = summaryview_height.constant + 40
        print("self.scrollVw.contentSize.heightwilllayout",self.scrollVw.contentSize.height)
         //   insidescroll_height.constant = summaryview_height.constant + 200
            self.view.layoutIfNeeded()
    }
    
    
        
        
   // MARK: - TableView Methods:
    
   func numberOfSections(in tableView: UITableView) -> Int {
       return 1
   }
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       if tableView == itemTblView
           {
             
       return itemArray.count
       }
      else if tableView == callllist_table
       {
        return phoneArr.count

       }
       
       else
       {
           return adonArray.count
       }
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
       
     if tableView == itemTblView
     {
       
       let cell = tableView.dequeueReusableCell(withIdentifier: "itemcell", for: indexPath) as! CartItemTblCell
       cell.selectionStyle = .none
      
           cell.line_lbl.isHidden = true
       cell.AdonTblView.separatorStyle = .none
       cell.AdonTblView.isScrollEnabled = false

       let dict = itemArray.object(at: indexPath.row) as! NSDictionary
       
       adonArray = dict.value(forKey: "addons") as! NSArray
       if adonArray.count == 0
       {
           cell.adonViewHeight.constant = 0
           cell.adonTbHeight.constant = 0
       }
       else
       {
           cell.adonTbHeight.constant = CGFloat(25 * adonArray.count)
           cell.adonViewHeight.constant = CGFloat((25 * adonArray.count) + 60)
       }
       
  cell.setTblViewDataSourceDelegate(dataSourceDelegate: self, forRow: indexPath.row)
       
       print("itemdict",dict)
       cell.productPriceLbl.text = "\(dict.value(forKey: "price") as! String)"
       cell.priceLbl.text = "Kr \(dict.value(forKey: "total_price") as! String)"
       cell.titleLbl.text = "\(dict.value(forKey: "name") as! String)"
        cell.qty_lbl.text = "\(dict.value(forKey: "quantity") as! String)x"
       return cell
       
     }
        else if tableView == callllist_table
        {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TransactionsCell
            cell.selectionStyle = .none
          let phonee = self.phoneArr[indexPath.row] as! String
            cell.priceLbl.text = "Call " + phonee
            return cell

     }
       else
     {
       
       let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AdonTblCell
              cell.selectionStyle = .none
       
      //   let dict = itemArray.object(at: indexPath.row) as! NSDictionary
             
    //   adonArray = dict.value(forKey: "addons") as! NSArray
       var adonDict = NSDictionary()
       
       if adonArray.count == 0
       {
           
       }
       else
       {
            adonDict = adonArray[indexPath.row] as! NSDictionary
           let name = adonDict.value(forKey: "name") as! String
                 let price = adonDict.value(forKey: "price") as! String
                 let qty = adonDict.value(forKey: "quantity") as! String
                 cell.adonTitle.text =  name + "(" + "kr" + price + ")"
           
           var qtyInt = Int(adonDict.value(forKey: "quantity") as! String)
           var priceDouble = Double(adonDict.value(forKey: "price") as! String)
           var totalPrice = Double(qtyInt!) * priceDouble!
           print("totalPricewithAdon",totalPrice)
           cell.adonPrice.text = "\(totalPrice)"
           
         
       }

  let totalCount = CGFloat(itemArray.count * 60 + adonArray.count * 25)
       print("totalCount",totalCount)
        
        
        print("tableViewHeight",tableViewHeight)

        
        itemTblHeight.constant = tableViewHeight
        summaryview_height.constant = tableViewHeight + vendornote_height.constant + 180
        self.scrollVw.contentSize.height = summaryview_height.constant + 40
        print("self.scrollVw.contentSize.height",self.scrollVw.contentSize.height)
        self.view.layoutIfNeeded()
       // self.view.lay

                  if totalCount > 300 {
                    // itemTblHeight.constant = 300
                  }
       else
                  {
                  // itemTblHeight.constant = CGFloat(itemArray.count * 60 + adonArray.count * 25)

       }
     
       //cell.adonPrice.text =    Double(adonDict.value(forKey: "price") as! String)
       
       return cell

       
       }
      // let totalCount = itemArray.count + adonArray.count
       
       
       
       
   }

        
    func deliveryStart()  {
                   let type = NVActivityIndicatorType.ballScaleRippleMultiple
                   let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                   let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColor, padding: 20)
                   self.view.addSubview(activityIndicatorView)
                   self.view.isUserInteractionEnabled = false

                   activityIndicatorView.startAnimating()
                   var param = [String: Any]()
               let userData = UserDefaultManager().getUserDefault(key: "userData") as? NSDictionary
               let orderDict = dict.value(forKey: "orderdata") as! NSDictionary

               param["driver_id"] = userData?.value(forKey: "id") as! String
        param["order_id"] = orderDict.value(forKey: "id") as! String

        WebService().postRequest(methodName: deliverystart, parameter: param) { (result) in
           activityIndicatorView.stopAnimating()
                       self.view.isUserInteractionEnabled = true

                       if let resultDict = result as? NSDictionary {
                           if resultDict.value(forKey: "status") as! Bool == true {
                   
                            self.action = "current"
                            self.managingButtonTitlebyAction()
                            
                           }
                           else
                           {
                          self.view.makeToast("Sorry,Not able to Start Service.")
                           }
                       }
                   }
           }
   func deliveryEnd()  {
                     let type = NVActivityIndicatorType.ballScaleRippleMultiple
                     let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                     let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColor, padding: 20)
                     self.view.addSubview(activityIndicatorView)
                     self.view.isUserInteractionEnabled = false

                     activityIndicatorView.startAnimating()
                     var param = [String: Any]()
                 let userData = UserDefaultManager().getUserDefault(key: "userData") as? NSDictionary
                 let orderDict = dict.value(forKey: "orderdata") as! NSDictionary

                 param["driver_id"] = userData?.value(forKey: "id") as! String
          param["order_id"] = orderDict.value(forKey: "id") as! String

          WebService().postRequest(methodName: deliveryend, parameter: param) { (result) in
             activityIndicatorView.stopAnimating()
                         self.view.isUserInteractionEnabled = true

                         if let resultDict = result as? NSDictionary {
                             if resultDict.value(forKey: "status") as! Bool == true {
                     self.action = "past"
                                self.managingButtonTitlebyAction()
                                
                             }
                             else
                             {
                            self.view.makeToast("Sorry,Not able to End Service.")
                             }
                         }
                     }
             }
    
    func ContactList()  {
                        let type = NVActivityIndicatorType.ballScaleRippleMultiple
                        let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                        let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColor, padding: 20)
                        self.view.addSubview(activityIndicatorView)
                        self.view.isUserInteractionEnabled = false

                        activityIndicatorView.startAnimating()
                        var param = [String: Any]()
                    let userData = UserDefaultManager().getUserDefault(key: "userData") as? NSDictionary
                    let orderDict = dict.value(forKey: "orderdata") as! NSDictionary

             param["type"] = "3"

             WebService().postRequest(methodName: contactdetail, parameter: param) { (result) in
                activityIndicatorView.stopAnimating()
                            self.view.isUserInteractionEnabled = true

                            if let resultDict = result as? NSDictionary {
                                if resultDict.value(forKey: "status") as! Bool == true {
                       
                                    
                                    if let newArr = resultDict.value(forKey: "data") as? NSArray
                                                           {
                                 self.phoneArr = newArr.value(forKey: "phone") as! NSArray
                    self.calltbl_height.constant = CGFloat(50 * self.phoneArr.count)
                            self.callList_popup.isHidden = false
                            self.callllist_table.delegate = self
                             self.callllist_table.dataSource = self
                        self.callllist_table.reloadData()
                                                            
                                                           }
                                    
                                   
                                }
                                else
                                {
                               self.view.makeToast("Sorry,Not able to End Service.")
                                }
                            }
                        }
                }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    if tableView == callllist_table
    {
        let phone = phoneArr[indexPath.row] as! String
        
               if let url = URL(string: "tel://\(phone)"),
                         UIApplication.shared.canOpenURL(url) {
                         if #available(iOS 10, *) {
                             UIApplication.shared.open(url, options: [:], completionHandler:nil)
                         } else {
                             UIApplication.shared.openURL(url)
                         }
                     } else {
                   self.view.makeToast("Call error")
                     }
        }
        
    }
    
    
    
    
    
    // MARK: - for calculating table height

    var tableViewHeight: CGFloat {
        itemTblView.layoutIfNeeded()

        return itemTblView.contentSize.height
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UILabel {
    var numberOfVisibleLines: Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(MAXFLOAT))
        let textHeight = sizeThatFits(maxSize).height
        let lineHeight = font.lineHeight
        return Int(ceil(textHeight / lineHeight))
    }
    
  

        func calculateMaxLines() -> Int {
            let maxSize = CGSize(width: frame.size.width, height: CGFloat(Float.infinity))
            let charSize = font.lineHeight
            let text = (self.text ?? "") as NSString
            let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
            let linesRoundedUp = Int(ceil(textSize.height/charSize))
            return linesRoundedUp
        }

   
    
}
