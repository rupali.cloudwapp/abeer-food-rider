//
//  EarningVC.swift
//  Abeer Food Rider
//
//  Created by USER on 10/10/20.
//  Copyright © 2020 Cloudwapp. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Toast_Swift

class EarningVC: UIViewController {

    
    var userDict = NSDictionary()
    // MARK: - Iboutlets
           
           
       @IBOutlet weak var balanceLbl: UILabel!
       
    @IBOutlet weak var recentTransLbl: UILabel!
    
    @IBOutlet weak var totaldeliveryorderLbl: UILabel!
    // MARK: - Ibactions
           
       
       
      
       @IBAction func CashoutAction(_ sender: Any) {
        
        
        let available_balance = self.userDict.value(forKey: "available_balance") as! String
            if available_balance == "0.00"
                                                            {
                                                                
                    self.view.makeToast("Sorry,You do not have available balance.")

                                                            }
        else
            {
        
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmCashoutVC") as! ConfirmCashoutVC
                vc.amountStr = available_balance
              self.navigationController?.pushViewController(vc, animated: true)
        }
        
       }
       
       @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
       }
    
    @IBAction func transactionsAction(_ sender: Any) {
       
        print("act")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TransactionsVC") as! TransactionsVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        showProfile()
    }
    
    func showProfile()  {
               let type = NVActivityIndicatorType.ballScaleRippleMultiple
               let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
               let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColor, padding: 20)
               self.view.addSubview(activityIndicatorView)
               self.view.isUserInteractionEnabled = false

               activityIndicatorView.startAnimating()
               var param = [String: Any]()
           let userData = UserDefaultManager().getUserDefault(key: "userData") as? NSDictionary
           
           param["driver_id"] = userData?.value(forKey: "id") as! String

               WebService().postRequest(methodName: showprofile, parameter: param) { (result) in
       activityIndicatorView.stopAnimating()
                   self.view.isUserInteractionEnabled = true

                   if let resultDict = result as? NSDictionary {
                       
                       print("resultDict",resultDict)
                       if resultDict.value(forKey: "status") as! Bool == true {
                           
                           if let userData = resultDict.value(forKey: "data") as? NSDictionary {
                            self.userDict = userData
                            
                            let available_balance = userData.value(forKey: "available_balance") as! String
                           
                            let lasttxn_amt = userData.value(forKey: "lasttxn_amt") as! String
                             let lasttxn_date = userData.value(forKey: "lasttxn_date") as! String
                                                                                
                            self.balanceLbl.text = "Kr " + available_balance
                            
                            self.recentTransLbl.text = "Recent transactions" + "\n" + lasttxn_amt + " " + lasttxn_date
                          
                          
                            
                           
                           }
                           }
                       else
                       {
                      self.view.makeToast("Sorry,Not able to show data.")
                       }
                   }
               }
       }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
