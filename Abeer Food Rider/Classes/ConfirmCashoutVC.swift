//
//  ConfirmCashoutVC.swift
//  Abeer Food Rider
//
//  Created by USER on 10/10/20.
//  Copyright © 2020 Cloudwapp. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Toast_Swift

class ConfirmCashoutVC: UIViewController {

    var amountStr = String()
    
     // MARK: - Iboutlets
        
        
    @IBOutlet weak var balanceLbl: UILabel!
    
    @IBOutlet weak var successPopup: UIView!
    
    // MARK: - Ibactions
        
    
    
    @IBAction func DoneAction(_ sender: Any) {
        self.successPopup.isHidden = true
        
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
    
    @IBAction func confirmCashoutAction(_ sender: Any) {
        confirmCashout()
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
   
    
    
    
    func confirmCashout()  {
                 let type = NVActivityIndicatorType.ballScaleRippleMultiple
                 let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                 let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColor, padding: 20)
                 self.view.addSubview(activityIndicatorView)
                 self.view.isUserInteractionEnabled = false

                 activityIndicatorView.startAnimating()
                 var param = [String: Any]()
             let userData = UserDefaultManager().getUserDefault(key: "userData") as? NSDictionary
             
             param["driver_id"] = userData?.value(forKey: "id") as! String
        param["amount"] = amountStr
        

                 WebService().postRequest(methodName: cashout, parameter: param) { (result) in
         activityIndicatorView.stopAnimating()
                     self.view.isUserInteractionEnabled = true

                     if let resultDict = result as? NSDictionary {
                         
                         print("resultDict",resultDict)
                         if resultDict.value(forKey: "status") as! Bool == true {
                         
                            self.successPopup.isHidden = false
                           
                             }
                         else
                         {
                        self.view.makeToast("Sorry,Not able to confirm cashout.")
                         }
                     }
                 }
         }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}




