//
//  loginVC.swift
//  Gofa Rider
//
//  Created by USER on 02/10/20.
//  Copyright © 2020 Cloudwapp. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import NVActivityIndicatorView
import Toast_Swift

@available(iOS 13.0, *)
class loginVC: UIViewController {
    
    
    // MARK: - IBdecS

    var eyetg = 0
    
    
    // MARK: - IBOUTLETS
    
    @IBOutlet weak var emailTxt: SkyFloatingLabelTextField!
   @IBOutlet weak var pwTxt: SkyFloatingLabelTextField!
    @IBOutlet weak var eyeBtn: UIButton!

    
    // MARK: - IBActions
    
    @available(iOS 14.0, *)
    @IBAction func signupBtnAction(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignupVC") as! SignupVC
                 self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
 @IBAction func forgotBtnAction(_ sender: Any) {
    
    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPWVC") as! ForgotPWVC
             self.navigationController?.pushViewController(vc, animated: true)
    
    }
    
  @IBAction func eyeBtnAction(_ sender: Any) {
    
    if eyetg == 0
    {
        eyeBtn.isSelected = true
        pwTxt.isSecureTextEntry = false
        eyetg = 1
    }
    else{
     eyeBtn.isSelected = false
        pwTxt.isSecureTextEntry = true

        eyetg = 0
    }
    
    }
    
    @IBAction func loginBtnAction(_ sender: Any) {
        
       if emailTxt.text!.isEmpty {
                 
        self.view.makeToast("Email required!!!")

             }
        else  if !((emailTxt.text?.isValidEmail())!) {
            self.view.makeToast("Valid Email required!!!")

        }
      else  if pwTxt.text!.isEmpty {
        
         self.view.makeToast("Password required!!!")

                    }
       else{
        
        loginDriver()
        
        }

    }
 
    override func viewDidLoad() {
        super.viewDidLoad()

        
        emailTxt.tintColor = themeColor
        pwTxt.tintColor = themeColor
        
//        emailTxt.text = "amit@gmail.com"
//        pwTxt.text = "123456"
        
        forStsBar()

   
    }
    func forStsBar()
        {
            if #available(iOS 13.0, *) {
                let app = UIApplication.shared
                let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                
                let statusbarView = UIView()
                statusbarView.backgroundColor = themeColorBrown
                view.addSubview(statusbarView)
              
                statusbarView.translatesAutoresizingMaskIntoConstraints = false
                statusbarView.heightAnchor
                    .constraint(equalToConstant: statusBarHeight).isActive = true
                statusbarView.widthAnchor
                    .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                statusbarView.topAnchor
                    .constraint(equalTo: view.topAnchor).isActive = true
                statusbarView.centerXAnchor
                    .constraint(equalTo: view.centerXAnchor).isActive = true
              
            } else {
                let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                statusBar?.backgroundColor = themeColorBrown
            }
  }
    
    
    // MARK: - WebService Calling

        func loginDriver()  {
            let type = NVActivityIndicatorType.ballScaleRippleMultiple
            let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
            let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColor, padding: 20)
            self.view.addSubview(activityIndicatorView)
            self.view.isUserInteractionEnabled = false
           

            let fcmToken = UserDefaults.standard.value(forKey: "self.fcmToken") as! String

            activityIndicatorView.startAnimating()
            var param = [String: Any]()
            param["email"] = self.emailTxt.text!
            param["password"] = self.pwTxt.text!
            param["fcm_token"] = fcmToken

            WebService().postRequest(methodName: login, parameter: param) { (result) in
    activityIndicatorView.stopAnimating()
                self.view.isUserInteractionEnabled = true

                if let resultDict = result as? NSDictionary {
                    if resultDict.value(forKey: "status") as! Bool == true {
                        if let userData = resultDict.value(forKey: "data") as? NSDictionary {
                            
                            UserDefaultManager().setUserDefault(value: userData, key: "userData")
                        
                            self.view.makeToast("Logged in Successfully!!")

                            
                  if #available(iOS 13.0, *) {
          let scene = UIApplication.shared.connectedScenes.first
           if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
               
            sd.setTabRootController()
            
              }
                 }
                   else  {
                             appDele.setTabRootController()
                                                                                }
                         
                        }
                    }
                    else
                    {
                        self.view.makeToast(resultDict.value(forKey: "message") as! String)

                //   self.view.makeToast("Driver not found")

                    }
                    
                    
                    
                }
            }
     

    }
    
    
    
    
    
}
