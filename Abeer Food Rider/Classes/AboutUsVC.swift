//
//  AboutUsVC.swift
//  Gofa User
//
//  Created by CW-21 on 18/05/20.
//  Copyright © 2020 CW-21. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

@available(iOS 13.0, *)
@available(iOS 14.0, *)

class AboutUsVC: UIViewController {

    let appDele = UIApplication.shared.delegate as! AppDelegate

    
    @IBOutlet weak var webview: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        ServicePrivacy()
    
        forStsBar()
                }
            func forStsBar()
            {
                if #available(iOS 13.0, *) {
                    let app = UIApplication.shared
                    let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                    
                    let statusbarView = UIView()
                    statusbarView.backgroundColor = themeColorBrown
                    view.addSubview(statusbarView)
                  
                    statusbarView.translatesAutoresizingMaskIntoConstraints = false
                    statusbarView.heightAnchor
                        .constraint(equalToConstant: statusBarHeight).isActive = true
                    statusbarView.widthAnchor
                        .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                    statusbarView.topAnchor
                        .constraint(equalTo: view.topAnchor).isActive = true
                    statusbarView.centerXAnchor
                        .constraint(equalTo: view.centerXAnchor).isActive = true
                  
                } else {
                    let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                    statusBar?.backgroundColor = themeColorBrown
                }
                
                
                }
    func ServicePrivacy()
    {
        let type = NVActivityIndicatorType.ballScaleRippleMultiple
        let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
        let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColor, padding: 20)
        self.view.addSubview(activityIndicatorView)
        
        self.view.isUserInteractionEnabled = false
        
        activityIndicatorView.startAnimating()
        var param = [String:Any]()
  
        WebService().postRequest(methodName: terms , parameter: [:]) { (response) in
            print("response",response)
            
            activityIndicatorView.stopAnimating()
            self.view.isUserInteractionEnabled = true
            
            if let newResponse = response as? NSDictionary {
                if newResponse.value(forKey: "status") as! Bool  == true {
                    
                    let descr = newResponse.value(forKey: "data") as! String
                    
                    // let descr = data.value(forKey: "about") as! String
                    let descriptionStr = descr
                    let descFinal = descriptionStr.replacingOccurrences(of: "(", with: "").replacingOccurrences(of: ")", with: "")
                    let descFinal2 = descFinal.replacingOccurrences(of: "\n", with: " ").replacingOccurrences(of: "\"", with: "")
                    //  let content = "<html><body><p><font size=30> \(descFinal2)</font></p></body></html>"
                    
                    // let test = String(content.filter { !" \n\t\r".contains(RM0) })
                    
                    //                        self.web_view.loadHTMLString(content, baseURL: nil)
                    //                        print("content",content)
                    // let descWithouthtml = descr.withoutHtml as! String
                    
                    let content = "<html><body><p><font size=4> \(descriptionStr)</font></p></body></html>"
                    
                    self.webview.loadHTMLString(content, baseURL: nil)
                    
                }
                else {
                    // self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)
                    
                }
            }
            else if let newMsg = response as? String{
                // self.view.showToast(toastMessage: newMsg, duration: 1)//
                
            }
            else {
                
                //  self.view.showToast(toastMessage: "No Data Found", duration: 1)
                
            }
        }
        
    }

    @IBAction func back_act(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
