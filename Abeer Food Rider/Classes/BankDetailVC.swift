//
//  BankDetailVC.swift
//  Abeer Food Rider
//
//  Created by USER on 04/11/20.
//  Copyright © 2020 Cloudwapp. All rights reserved.
//

import UIKit
import NVActivityIndicatorView


class BankDetailVC: UIViewController {
    
    @IBOutlet weak var NmLbl: UILabel!
       @IBOutlet weak var NumberLbl: UILabel!
       @IBOutlet weak var accLbl: UILabel!
       @IBOutlet weak var SCROLL_VIEW: UIScrollView!
    @IBOutlet weak var profile_img: UIImageView!

          
          // MARK: - IBActions
          
          @IBAction func logoutBtnAction(_ sender: Any) {
       
           
           
         //  setUserDefault(value: userData, key: "userData")

       }
       @IBAction func BACKAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    
    }

        override func viewDidLoad() {
            super.viewDidLoad()

                 forStsBar()
            
            
            
              }
        override func viewWillAppear(_ animated: Bool) {
                    forSettingDeatils()

        }
        
        
        
        override func viewDidLayoutSubviews() {
            SCROLL_VIEW.contentSize.height = 550
        }
         func forSettingDeatils()
         {
            DriverBankDetails()

            
        }
        
        
              func forStsBar()
                  {
                      if #available(iOS 13.0, *) {
                          let app = UIApplication.shared
                          let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                          
                          let statusbarView = UIView()
                          statusbarView.backgroundColor = themeColorBrown
                          view.addSubview(statusbarView)
                        
                          statusbarView.translatesAutoresizingMaskIntoConstraints = false
                          statusbarView.heightAnchor
                              .constraint(equalToConstant: statusBarHeight).isActive = true
                          statusbarView.widthAnchor
                              .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                          statusbarView.topAnchor
                              .constraint(equalTo: view.topAnchor).isActive = true
                          statusbarView.centerXAnchor
                              .constraint(equalTo: view.centerXAnchor).isActive = true
                        
                      } else {
                          let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                          statusBar?.backgroundColor = themeColorBrown
                      }
            }
    func DriverBankDetails()  {
             let type = NVActivityIndicatorType.ballScaleRippleMultiple
             let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
             let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColor, padding: 20)
             self.view.addSubview(activityIndicatorView)
             self.view.isUserInteractionEnabled = false

             activityIndicatorView.startAnimating()
             var param = [String: Any]()
         let userData = UserDefaultManager().getUserDefault(key: "userData") as? NSDictionary
         
         param["driver_id"] = userData?.value(forKey: "id") as! String

             WebService().postRequest(methodName: showprofile, parameter: param) { (result) in
     activityIndicatorView.stopAnimating()
                 self.view.isUserInteractionEnabled = true

                 if let resultDict = result as? NSDictionary {
                     
                     print("resultDict",resultDict)
                     if resultDict.value(forKey: "status") as! Bool == true {
                         
                         if let newDict = resultDict.value(forKey: "data") as? NSDictionary {
   self.accLbl.text = newDict.value(forKey: "acc_no") as! String
                            let code = ""
                            let mobile = newDict.value(forKey: "phone") as! String
    

                            self.NumberLbl.text = newDict.value(forKey: "clearing_no") as! String
                            self.NmLbl.text = newDict.value(forKey: "bank_name") as! String
                         }
                        
                         }
                     else
                     {
                    self.view.makeToast("Sorry,Not able to update profile.")
                     }
                 }
             }
     }
    

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
