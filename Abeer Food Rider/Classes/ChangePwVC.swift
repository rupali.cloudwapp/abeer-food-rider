//
//  ChangePwVC.swift
//  Abeer Food Rider
//
//  Created by USER on 04/11/20.
//  Copyright © 2020 Cloudwapp. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import NVActivityIndicatorView

@available(iOS 13.0, *)
class ChangePwVC: UIViewController {

    
  
    @IBOutlet weak var oldpw_tf: SkyFloatingLabelTextField!
    
    @IBOutlet weak var newpw_tf: SkyFloatingLabelTextField!
    
    @IBOutlet weak var confirmpw_tf: SkyFloatingLabelTextField!
    @IBOutlet weak var profile_img: UIImageView!

    
       @IBOutlet weak var SCROLL_VIEW: UIScrollView!

          
          // MARK: - IBActions
          
          @IBAction func updateBtnAction(_ sender: Any) {
       
    if oldpw_tf.text!.isEmpty {
               
                self.view.makeToast("Old Password required!!!")

                           }
           else if newpw_tf.text!.isEmpty {
            
             self.view.makeToast("New Password required!!!")

                        }
        
        else if confirmpw_tf.text!.isEmpty {
                   
                    self.view.makeToast("Confirm Password required!!!")

                               }
    else if newpw_tf.text! != confirmpw_tf.text! {
                       
                        self.view.makeToast("Confirm Password not matched!!!")

                                   }
    else{
        Changepw()
            }
        

       }
    
    
       @IBAction func BACKAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        forStsBar()
             
             
             
               }
         override func viewWillAppear(_ animated: Bool) {
            SCROLL_VIEW.contentSize.height = 550

         }
         
         override func viewDidLayoutSubviews() {
             SCROLL_VIEW.contentSize.height = 550
         }
        
         
         
               func forStsBar()
                   {
                       if #available(iOS 13.0, *) {
                           let app = UIApplication.shared
                           let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                           
                           let statusbarView = UIView()
                           statusbarView.backgroundColor = themeColorBrown
                           view.addSubview(statusbarView)
                         
                           statusbarView.translatesAutoresizingMaskIntoConstraints = false
                           statusbarView.heightAnchor
                               .constraint(equalToConstant: statusBarHeight).isActive = true
                           statusbarView.widthAnchor
                               .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                           statusbarView.topAnchor
                               .constraint(equalTo: view.topAnchor).isActive = true
                           statusbarView.centerXAnchor
                               .constraint(equalTo: view.centerXAnchor).isActive = true
                         
                       } else {
                           let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                           statusBar?.backgroundColor = themeColorBrown
                       }
             }
    // MARK: - WebService Calling

           func Changepw()  {
               let type = NVActivityIndicatorType.ballScaleRippleMultiple
               let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
               let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColor, padding: 20)
               self.view.addSubview(activityIndicatorView)
               self.view.isUserInteractionEnabled = false
              

               let fcmToken = UserDefaults.standard.value(forKey: "self.fcmToken") as! String

               activityIndicatorView.startAnimating()
               var param = [String: Any]()
            
            let userData = UserDefaultManager().getUserDefault(key: "userData") as? NSDictionary
               
               param["driver_id"] = userData?.value(forKey: "id") as! String
               param["oldpassword"] = self.oldpw_tf.text!
               param["newpassword"] = self.newpw_tf.text!

               WebService().postRequest(methodName: change_password, parameter: param) { (result) in
       activityIndicatorView.stopAnimating()
                   self.view.isUserInteractionEnabled = true

                   if let resultDict = result as? NSDictionary {
                       if resultDict.value(forKey: "status") as! Bool == true {
                           

                               
                               
                     if #available(iOS 13.0, *) {
             let scene = UIApplication.shared.connectedScenes.first
              if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                  
               sd.setLoginrPage()
               
                 }
                    }
                      else  {
                        
                      appDele.setLoginrPage()
                                                                                   }
                            
                           
                       }
                       else
                       {
                         
            if let message = resultDict.value(forKey: "message") as? String {
            self.view.makeToast(resultDict.value(forKey: "message") as! String)
             }
                        else
                        {                      self.view.makeToast("Some error to chnage your password!!")
}

                       }
                    
                   }
               }
        

       }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
