//
//  ForgotPWVC.swift
//  Abeer Food Rider
//
//  Created by USER on 09/10/20.
//  Copyright © 2020 Cloudwapp. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import NVActivityIndicatorView
import Toast_Swift

@available(iOS 13.0, *)
@available(iOS 13.0, *)
class ForgotPWVC: UIViewController {
    
     // MARK: - IBOUTLETS:
     

      @IBOutlet weak var emailTxt: SkyFloatingLabelTextField!
     

     // MARK: - IBActions
      
     @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

      }
    @IBAction func resetBtnAction(_ sender: Any) {
        
        if emailTxt.text!.isEmpty {
                 
        self.view.makeToast("Email required!!!")

             }
        else  if !((emailTxt.text?.isValidEmail())!) {
            self.view.makeToast("Valid Email required!!!")

        }
        else
        {
            
            forgotPasswordCalling()
        }
        
        
         }

    override func viewDidLoad() {
        super.viewDidLoad()

     forStsBar()

          
           }
           func forStsBar()
               {
                   if #available(iOS 13.0, *) {
                       let app = UIApplication.shared
                       let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                       
                       let statusbarView = UIView()
                       statusbarView.backgroundColor = themeColorBrown
                       view.addSubview(statusbarView)
                     
                       statusbarView.translatesAutoresizingMaskIntoConstraints = false
                       statusbarView.heightAnchor
                           .constraint(equalToConstant: statusBarHeight).isActive = true
                       statusbarView.widthAnchor
                           .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                       statusbarView.topAnchor
                           .constraint(equalTo: view.topAnchor).isActive = true
                       statusbarView.centerXAnchor
                           .constraint(equalTo: view.centerXAnchor).isActive = true
                     
                   } else {
                       let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                       statusBar?.backgroundColor = themeColorBrown
                   }
         }
    // MARK: - WebService Calling

           func forgotPasswordCalling()  {
               let type = NVActivityIndicatorType.ballScaleRippleMultiple
               let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
               let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColor, padding: 20)
               self.view.addSubview(activityIndicatorView)
               self.view.isUserInteractionEnabled = false
              


               activityIndicatorView.startAnimating()
               var param = [String: Any]()
               param["email"] = self.emailTxt.text!
               param["user_type"] = "Driver"

               WebService().postRequest(methodName: forgot, parameter: param) { (result) in
       activityIndicatorView.stopAnimating()
                   self.view.isUserInteractionEnabled = true

                   if let resultDict = result as? NSDictionary {
                       if resultDict.value(forKey: "status") as! Bool == true {
                      
                        self.view.makeToast("Email Send Successfully,please check to reset your password.")
                       
                        self.emailTxt.text = ""
                        
                       }
                       else
                       {
                       
                      self.view.makeToast("Sorry,failed to sent email.")

                       }
                       
                       
                       
                   }
               }
        

       }
       
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
