//
//  TabBarVC.swift
//  Oohhsito
//
//  Created by CW on 03/07/19.
//  Copyright © 2019 CW. All rights reserved.
//

import UIKit

class TabBarVC: UITabBarController,UITabBarControllerDelegate {

    override func viewWillLayoutSubviews() {
        
        // for corner radius
        self.tabBar.layer.masksToBounds = true
                  self.tabBar.isTranslucent = true
                  self.tabBar.layer.borderWidth = 0
             self.delegate = self
        self.delegate = self
    
        let screenSize: CGRect = UIScreen.main.bounds

        var tabFrame = self.tabBar.frame
   
        self.tabBar.frame = tabFrame
        self.tabBar.backgroundColor = UIColor.white
        
        let tabBarItems = tabBar.items! as [UITabBarItem]
        tabBarItems[0].title = nil
        tabBarItems[0].imageInsets = UIEdgeInsets(top: 5,left: -10,bottom: -5,right:0)
        
        tabBarItems[1].title = nil
        tabBarItems[1].imageInsets = UIEdgeInsets(top: 5,left: -10,bottom: -5,right: 0)
        
    
    }
    
       override func viewWillAppear(_ animated: Bool) {
        UITabBar.appearance().backgroundImage = UIImage(named: "plain-white-background")

    }
}

extension UIImage {
    
    class func colorForNavBar(color: UIColor) -> UIImage {
        //let rect = CGRectMake(0.0, 0.0, 1.0, 1.0)
        
        let rect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: 1.0, height: 1.0))
        
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
}
