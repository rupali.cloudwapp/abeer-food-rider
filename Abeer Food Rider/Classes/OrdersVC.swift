//
//  OrdersVC.swift
//  Gofa Rider
//
//  Created by USER on 02/10/20.
//  Copyright © 2020 Cloudwapp. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Toast_Swift
import CoreLocation
import AVFoundation


class OrdersVC: UIViewController,UITableViewDelegate,UITableViewDataSource,CLLocationManagerDelegate {
    
    var audioPlayer = AVAudioPlayer()

    
    // MARK: - IBDeclarations :
    
   var cancelOrderid = String()
    var cancelOrderStore = String()
    
    var counter = 0

    var topassLat = String()
    var topassLong = String()
    var addressStr = String()
var timer : Timer?

    var OrdersArr = NSArray()
    var refreshControl = UIRefreshControl()
var dateStr = String()
    var dateShwn = Date()
    var datetodayStr = String()
    var orderidStr = String()
    var locationManager = CLLocationManager()

    // MARK: - IOBOutlets :
    
    
    @IBOutlet weak var cancelOrder_popup: UIView!
    @IBOutlet weak var acceptOrder_popup: UIView!
    
    @IBOutlet weak var cancelOrderid_lbl: UILabel!
    
    @IBOutlet weak var cancelStore_lbl: UILabel!
    
    @IBOutlet weak var datevw_hright: NSLayoutConstraint!
    
    @IBOutlet weak var date_view: UIView!
    
    @IBOutlet weak var date_lbl: UILabel!
    
    
    @IBOutlet weak var newOrdersLbl: UILabel!
    @IBOutlet weak var newUnderlineLbl: UILabel!
    @IBOutlet weak var ongoingOrdersLbl: UILabel!
   @IBOutlet weak var ongoingUnderlineLbl: UILabel!
    @IBOutlet weak var pastOrdersLbl: UILabel!
    @IBOutlet weak var pastUnderlineLbl: UILabel!
    @IBOutlet weak var toggleSwitch: UISwitch!
    @IBOutlet weak var OnlineLbl: UILabel!
    @IBOutlet weak var OrdersTbl: UITableView!
    
    // MARK: - IBActions:

    @IBAction func acceptOrderAction(_ sender: Any) {
        audioPlayer.pause()
        audioPlayer.stop()
        AcceptOrder()
        
    }
    @IBAction func acceptResignAction(_ sender: Any) {
        
        
    }
    
    @IBAction func cancelOkAction(_ sender: Any) {
        
        self.cancelOrder_popup.isHidden = true
          toRefresh()
        
    }
    @IBAction func previousAction(_ sender: Any) {
        let previousdate = Calendar.current.date(byAdding: .day, value: -1, to: dateShwn)!
        dateShwn = previousdate
         print("previousdate",previousdate)
        let formatter = DateFormatter()
        
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: dateShwn)
        dateStr = result
        self.date_lbl.text = result
        self.pastorderByFilter()

        
    }
    @IBAction func nextAction(_ sender: Any) {
        
        
        if date_lbl.text == datetodayStr
        {}
        else
        {
        
        let nxtdate = Calendar.current.date(byAdding: .day, value: 1, to: dateShwn)!
               dateShwn = nxtdate
        
                print("nxtdate",nxtdate)
        let formatter = DateFormatter()
        
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: dateShwn)
        dateStr = result
        self.date_lbl.text = result
            self.pastorderByFilter()

        }
        
    }
    @IBAction func newUnderlineAction(_ sender: Any) {
        ForNewOrders()
    }
    @IBAction func ongoingUnderlineAction(_ sender: Any) {
         ForOngoingOrders()
    }
    @IBAction func pastUnderlineAction(_ sender: Any) {
        ForPastOrders()
    }
    @IBAction func toggleSwitchAction(_ sender: UIButton) {
        
         sender.isSelected = !sender.isSelected

        if OnlineLbl.text == "Offline"
           {
               print("on")
            OnlineLbl.text = "Online"
            toggleSwitch.isOn = true

            DriverOnlineOffline(action: "online")
            
          
            
        
           }
           else {
               print("off")
            OnlineLbl.text = "Offline"
            toggleSwitch.isOn = false

            DriverOnlineOffline(action: "offline")
             

           }
        
    }
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

              forStsBar()
        
//        locationManager = CLLocationManager()
//               locationManager.delegate = self;
//               locationManager.desiredAccuracy = kCLLocationAccuracyBest
//               locationManager.requestAlwaysAuthorization()
//        locationManager.startUpdatingLocation()
        
        

        if CLLocationManager.locationServicesEnabled(){
                 
//                 switch CLLocationManager.authorizationStatus() {
//                 case .notDetermined, .restricted, .denied:
//
//                     let alert = UIAlertController(title: "Location Access is Turned Off", message: "In order to locate your position, please open settings and set location access to 'While Using the App'", preferredStyle: .alert)
//                     let openAction = UIAlertAction(title: "Settings", style: .default, handler: { (alertAction) in
//
//                         if let url = URL(string: UIApplication.openSettingsURLString)  {
//                             UIApplication.shared.open(url as URL, options: [:], completionHandler: { (success) in
//                                 print(success)
//                             })
//                         }
//                     })
//
//                     alert.addAction(openAction)
//                     self.present(alert, animated: true, completion: nil)
//
//                 case .authorizedAlways, .authorizedWhenInUse:
//                     print("Access")
//                     locationManager.requestWhenInUseAuthorization()
//                     locationManager.desiredAccuracy = kCLLocationAccuracyBest
//                     locationManager.startMonitoringSignificantLocationChanges()
//                     locationManager.startUpdatingLocation()
//                 }
             }
             else {
                 let alert = UIAlertController(title: "Location Access is Turned Off", message: "In order to locate your position, please open settings and set location access to 'While Using the App'", preferredStyle: .alert)
                 let openAction = UIAlertAction(title: "Settings", style: .default, handler: { (alertAction) in
                     
                     if let url = URL(string: UIApplication.openSettingsURLString)  {
                         UIApplication.shared.open(url as URL, options: [:], completionHandler: { (success) in
                             print(success)
                         })
                     }
                   })
                 
                 alert.addAction(openAction)
                 self.present(alert, animated: true, completion: nil)
             }
        
        
    
        
        
        
        
        let alertSound = URL(fileURLWithPath: Bundle.main.path(forResource: "iphone_tone", ofType: "mp3")!)
                 print(alertSound)
                 try! AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
                 try! AVAudioSession.sharedInstance().setActive(true)
                 try! audioPlayer = AVAudioPlayer(contentsOf: alertSound)
                 audioPlayer.prepareToPlay()
        
        self.OrdersTbl.bounces = true
        
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to Refresh Orders")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        self.OrdersTbl.addSubview(refreshControl) // not required when using UITableViewController

        //  right:
                                 
      let right = UISwipeGestureRecognizer(target : self, action : #selector(OrdersVC.rightSwipe))
      right.direction = .right
      self.OrdersTbl.addGestureRecognizer(right)
                                 
      //  left:
     let left = UISwipeGestureRecognizer(target : self, action : #selector(OrdersVC.leftSwipe))
      left.direction = .left
      self.OrdersTbl.addGestureRecognizer(left)
        
        
        
//        if UserDefaults.standard.bool(forKey: "notiAppear") == true
//         {
//            print("inside true")
//
//                 self.ShowAcceptpopup()
//         }
//        else
//         {
//        UserDefaults.standard.set(false, forKey: "notiAppear")
//
//
//        }
        
        
        
        
        
        
          }
    @objc func refresh(_ sender: AnyObject) {

        if newOrdersLbl.textColor == .white
                           {
                            DriverOrders(action: "New")

                           }
                           else if ongoingOrdersLbl.textColor == .white
                           {
                          DriverOrders(action: "Current")
                           }
                           else
                           {
                               DriverOrders(action: "Completed")
                           }


    }
//MARK : SHOWN POPUP ON NOTIFICARTIONS:
    
    func ShowCancelOrderpopup()
      {
 
        self.cancelStore_lbl.text = "(" + self.cancelOrderStore + ")"
        self.cancelOrderid_lbl.text =  "OrderId" + self.cancelOrderid
        
       self.cancelOrder_popup.isHidden = false

       }
    
   func ShowAcceptpopup()
   {

    if timer == nil {
                
              timer = Timer.scheduledTimer(timeInterval:1, target:self, selector:#selector(prozessTimer), userInfo: nil, repeats: true)
           }

    self.acceptOrder_popup.isHidden = false
    audioPlayer.play()


    }
    func stopTimer()
       {
         if timer != nil {
           timer!.invalidate()
           timer = nil
         }
        
        UserDefaults.standard.set(false, forKey: "notiAppear")
        
       }
    @objc func prozessTimer() {
        counter += 1
        print("This is a second ", counter)
        if counter >= 60 * 5
        {
            self.acceptOrder_popup.isHidden = true
            audioPlayer.stop()
            
            stopTimer()
            UserDefaults.standard.set(false, forKey: "notiAppear")

           // counter = 0

        }
       else
        {
            audioPlayer.play()
            self.acceptOrder_popup.isHidden = false

        }

    }
    
    
    
    func toRefresh()
    {
        
        if newOrdersLbl.textColor == .white
                                 {
                                  DriverOrders(action: "New")

                                 }
                                 else if ongoingOrdersLbl.textColor == .white
                                 {
                                DriverOrders(action: "Current")
                                 }
                                 else
                                 {
                                     DriverOrders(action: "Completed")
                                 }
        
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        let current = UNUserNotificationCenter.current()

        current.getNotificationSettings(completionHandler: { (settings) in
            if settings.authorizationStatus == .notDetermined {
                
                let alert = UIAlertController(title: "Notification for Abeer Food Rider is Turned Off", message: "In order to get the Notifications, please open settings and allow the notifications", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Settings", style: .default) { (action) in
                    
                    if let url = URL(string: UIApplication.openSettingsURLString)  {
                        UIApplication.shared.open(url as URL, options: [:], completionHandler: { (success) in
                            print(success)
                        })
                    }
                }
               // let noAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                
                alert.addAction(okAction)
              //  alert.addAction(noAction)
                DispatchQueue.global(qos: .userInitiated).async {
                    DispatchQueue.main.async {
                self.present(alert, animated: true, completion: nil)
                    }
                }
                
            } else if settings.authorizationStatus == .denied {
               
                
                let alert = UIAlertController(title: "Notification for Abeer Food Rider is Turned Off", message: "In order to get the Notifications, please open settings and allow the notifications", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Settings", style: .default) { (action) in
                    
                    if let url = URL(string: UIApplication.openSettingsURLString)  {
                        UIApplication.shared.open(url as URL, options: [:], completionHandler: { (success) in
                            print(success)
                        })
                    }
                }
               // let noAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                
                alert.addAction(okAction)
              //  alert.addAction(noAction)
                DispatchQueue.global(qos: .userInitiated).async {
                    DispatchQueue.main.async {
                self.present(alert, animated: true, completion: nil)
                    }
                }
                
                
            } else if settings.authorizationStatus == .authorized {
                // Notification permission was already granted
            }
        })
       
        
        timer = nil
        
        print("notiappearBool", UserDefaults.standard.bool(forKey: "notiAppear"))

        
        if UserDefaults.standard.bool(forKey: "notiAppear") == true
                {
            print("inside true")
            counter = UserDefaults.standard.value(forKey: "counterStr") as! Int
                        self.ShowAcceptpopup()
                }
               else
                {
              UserDefaults.standard.set(false, forKey: "notiAppear")
               // stopTimer()
               
               }
        
        
        
        if let newDict = UserDefaultManager().getUserDefault(key: "userData") as? NSDictionary {
                               
                               print("newDict",newDict)
                let status = newDict.value(forKey: "is_online") as! String
            if status == "ONLINE"
            {
                OnlineLbl.text = "Online"
                toggleSwitch.isOn = true
            
            }
            else
            {
                OnlineLbl.text = "Offline"
                toggleSwitch.isOn = false

            }
                
                               
               }
        
        
              locationManager = CLLocationManager()
                     locationManager.delegate = self;
                     locationManager.desiredAccuracy = kCLLocationAccuracyBest
                     locationManager.requestAlwaysAuthorization()
              locationManager.startUpdatingLocation()
              
        
    ForNewOrders()
        
        
       
      
        

    }
   // MARK: - gesturesAction
    @objc
       func rightSwipe(){
        
       if newOrdersLbl.textColor == .white
                    {
                    ForPastOrders()
                    }
                    else if ongoingOrdersLbl.textColor == .white
                    {
                   ForNewOrders()
                    }
                    else
                    {
                        ForOngoingOrders()
                    }
        
    }
    
    @objc
         func leftSwipe(){
        if newOrdersLbl.textColor == .white
               {
               ForOngoingOrders()
               }
               else if ongoingOrdersLbl.textColor == .white
               {
              ForPastOrders()
               }
               else
               {
                   ForNewOrders()
               }
      
          
      }
    
   func forStsBar()
              {
                  if #available(iOS 13.0, *) {
                      let app = UIApplication.shared
                      let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                      
                      let statusbarView = UIView()
                      statusbarView.backgroundColor = themeColorBrown
                      view.addSubview(statusbarView)
                    
                      statusbarView.translatesAutoresizingMaskIntoConstraints = false
                      statusbarView.heightAnchor
                          .constraint(equalToConstant: statusBarHeight).isActive = true
                      statusbarView.widthAnchor
                          .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                      statusbarView.topAnchor
                          .constraint(equalTo: view.topAnchor).isActive = true
                      statusbarView.centerXAnchor
                          .constraint(equalTo: view.centerXAnchor).isActive = true
                    
                  } else {
                      let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                      statusBar?.backgroundColor = themeColorBrown
                  }
        }
    
 func ForNewOrders()
 {
    
    newOrdersLbl.textColor = .white
    newUnderlineLbl.backgroundColor = .white
    ongoingOrdersLbl.textColor = .gray
       ongoingUnderlineLbl.isHidden = true
    pastOrdersLbl.textColor = .gray
       pastUnderlineLbl.isHidden = true
    newUnderlineLbl.isHidden = false
    DriverOrders(action: "New")
    self.datevw_hright.constant = 0
          self.date_view.isHidden = true
    }
    
    func ForOngoingOrders()
    {
       newOrdersLbl.textColor = .gray
         newUnderlineLbl.isHidden = true
         ongoingOrdersLbl.textColor = .white
            ongoingUnderlineLbl.backgroundColor = .white
         pastOrdersLbl.textColor = .gray
            pastUnderlineLbl.isHidden = true
       ongoingUnderlineLbl.isHidden = false
        DriverOrders(action: "Current")
        self.datevw_hright.constant = 0
        self.date_view.isHidden = true

       }
    func ForPastOrders()
    {
       
       newOrdersLbl.textColor = .gray
              newUnderlineLbl.isHidden = true
              ongoingOrdersLbl.textColor = .gray
                 ongoingUnderlineLbl.isHidden = true
              pastOrdersLbl.textColor = .white
                 pastUnderlineLbl.backgroundColor = .white
       pastUnderlineLbl.isHidden = false
        DriverOrders(action: "Completed")
        self.datevw_hright.constant = 40
        self.date_view.isHidden = false
        let date = Date()
        dateShwn = date
        let formatter = DateFormatter()
        
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: date)
        dateStr = result
        datetodayStr = result
        self.date_lbl.text = result
        
        self.pastorderByFilter()

        
        
       }
    func AcceptOrder()  {
               let type = NVActivityIndicatorType.ballScaleRippleMultiple
               let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
               let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColor, padding: 20)
               self.view.addSubview(activityIndicatorView)
               self.view.isUserInteractionEnabled = false

               activityIndicatorView.startAnimating()
               var param = [String: Any]()
           let userData = UserDefaultManager().getUserDefault(key: "userData") as? NSDictionary
          param["driver_id"] = userData?.value(forKey: "id") as! String
        let ordid = self.orderidStr
        
       // UserDefaults.standard.set(self.orderidStr, forKey: "self.orderidStr")

        
        param["order_id"] = UserDefaults.standard.value(forKey: "self.orderidStr")
           
        print("acceptParam",param)
               WebService().postRequest(methodName: accept, parameter: param) { (result) in
       activityIndicatorView.stopAnimating()
                   self.view.isUserInteractionEnabled = true

                   if let resultDict = result as? NSDictionary {
                    UserDefaults.standard.set(false, forKey: "notiAppear")

                       print("resultDict",resultDict)
                     if resultDict.value(forKey: "status") as! Bool == true {
                    self.view.makeToast("Order accepted")
                        
                        self.toRefresh()
                        self.acceptOrder_popup.isHidden = true
                        self.audioPlayer.pause()
                        self.audioPlayer.stop()
                        self.stopTimer()


                    }
                  else
                     {
                        self.view.makeToast("Sorry,Not able to accept order.")
                        self.toRefresh()
                                             self.acceptOrder_popup.isHidden = true
                                             self.audioPlayer.pause()
                                             self.audioPlayer.stop()
                                             self.stopTimer()
                    }
                    
                           }
                       else
                       {
                      self.view.makeToast("Sorry,Not able to accept order.")
                        self.acceptOrder_popup.isHidden = true
                                            self.audioPlayer.pause()
                                            self.audioPlayer.stop()
                                            self.stopTimer()
                                            
                       }
                   }
               }

    
    func DriverOnlineOffline(action:String)  {
            let type = NVActivityIndicatorType.ballScaleRippleMultiple
            let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
            let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColor, padding: 20)
            self.view.addSubview(activityIndicatorView)
            self.view.isUserInteractionEnabled = false

            activityIndicatorView.startAnimating()
            var param = [String: Any]()
        let userData = UserDefaultManager().getUserDefault(key: "userData") as? NSDictionary
        
        param["driver_id"] = userData?.value(forKey: "id") as! String
          param["action"] = action

            WebService().postRequest(methodName: onlineOffline, parameter: param) { (result) in
    activityIndicatorView.stopAnimating()
                self.view.isUserInteractionEnabled = true

                if let resultDict = result as? NSDictionary {
                    
                    print("resultDict",resultDict)
                    if resultDict.value(forKey: "status") as! Bool == true {
                        
                        if let userData = resultDict.value(forKey: "data") as? NSDictionary {
                                                   
                                                   UserDefaultManager().setUserDefault(value: userData, key: "userData")
                        
                        }
                        }
                    else
                    {
                   self.view.makeToast("Sorry,Not able to update status.")
                    }
                }
            }
    }
  func DriverOrders(action:String)  {
            let type = NVActivityIndicatorType.ballScaleRippleMultiple
            let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
            let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColor, padding: 20)
            self.view.addSubview(activityIndicatorView)
            self.view.isUserInteractionEnabled = false

            activityIndicatorView.startAnimating()
            var param = [String: Any]()
        let userData = UserDefaultManager().getUserDefault(key: "userData") as? NSDictionary
        
        param["driver_id"] = userData?.value(forKey: "id") as! String
          param["action"] = action

            WebService().postRequest(methodName: myorders, parameter: param) { (result) in
    activityIndicatorView.stopAnimating()
                self.view.isUserInteractionEnabled = true

                if let resultDict = result as? NSDictionary {
                    if resultDict.value(forKey: "status") as! Bool == true {
                        
                        print("resultDict",resultDict)
                     
                        if let newArr = resultDict.value(forKey: "data") as? NSArray
                        {
                            self.OrdersArr = newArr
                            self.OrdersTbl.delegate = self
                            self.OrdersTbl.dataSource = self
                            self.OrdersTbl.reloadData()
                        }
                        
                        self.refreshControl.endRefreshing()
            self.OrdersTbl.isHidden = false

                    }
                    else
                    {
                        self.refreshControl.endRefreshing()
                        self.OrdersTbl.isHidden = true

                   //self.view.makeToast("Sorry,Not able to update status.")
                    }
                }
            }
    }
    func pastorderByFilter()  {
            let type = NVActivityIndicatorType.ballScaleRippleMultiple
            let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
            let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColor, padding: 20)
            self.view.addSubview(activityIndicatorView)
            self.view.isUserInteractionEnabled = false

            activityIndicatorView.startAnimating()
            var param = [String: Any]()
        let userData = UserDefaultManager().getUserDefault(key: "userData") as? NSDictionary
        
        param["driver_id"] = userData?.value(forKey: "id") as! String
        param["date"] = self.dateStr

print("param",param)

            WebService().postRequest(methodName: filterbydate, parameter: param) { (result) in
    activityIndicatorView.stopAnimating()
                self.view.isUserInteractionEnabled = true

                if let resultDict = result as? NSDictionary {
                    if resultDict.value(forKey: "status") as! Bool == true {
                        
                        print("resultDict",resultDict)
                      
                        
                        
                     
                        if let newArr = resultDict.value(forKey: "data") as? NSArray
                        {
                            self.OrdersArr = newArr
                            self.OrdersTbl.delegate = self
                            self.OrdersTbl.dataSource = self
                            self.OrdersTbl.reloadData()
                        }
                        self.OrdersTbl.isHidden = false

                        self.refreshControl.endRefreshing()
            
                    }
                
                    else
                    {
                       self.OrdersTbl.isHidden = true
                        self.refreshControl.endRefreshing()

                   //self.view.makeToast("Sorry,Not able to update status.")
                    }
                }
            }
    }
      func updateDriverLocation()  {
                let type = NVActivityIndicatorType.ballScaleRippleMultiple
                let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColor, padding: 20)
                self.view.addSubview(activityIndicatorView)
                self.view.isUserInteractionEnabled = false

             //   activityIndicatorView.startAnimating()
                var param = [String: Any]()
            let userData = UserDefaultManager().getUserDefault(key: "userData") as? NSDictionary
            
            param["driver_id"] = userData?.value(forKey: "id") as! String
            param["current_lat"] = self.topassLat
        param["current_lon"] = self.topassLong
        param["address"] = self.addressStr


    print("updateparam",param)

                WebService().postRequest(methodName: location_update, parameter: param) { (result) in
       // activityIndicatorView.stopAnimating()
                    self.view.isUserInteractionEnabled = true

                    if let resultDict = result as? NSDictionary {
                        if resultDict.value(forKey: "status") as! Bool == true {
                            
                            print("updateresultDict",resultDict)
                         
                
                        }
                        else
                        {

                       //self.view.makeToast("Sorry,Not able to update status.")
                        }
                    }
                }
        }
    // MARK: - Tableview Delegate Datasource Methods :
    
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 125
        }
    
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.OrdersArr.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            let cell = OrdersTbl.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! OrderCell
            cell.selectionStyle = .none
            cell.viewOrderBtn.isUserInteractionEnabled = false
            
            let dict = self.OrdersArr[indexPath.row] as! NSDictionary
            let storeDict = dict.value(forKey: "storedetail") as! NSDictionary
           let orderDict = dict.value(forKey: "orderdata") as! NSDictionary
            let menuDict = dict.value(forKey: "menudetail") as! NSArray

            cell.name.text = storeDict.value(forKey: "name") as! String
            let price = menuDict.value(forKey: "total_price") as! NSArray
            let pricee = price[0] as! String
            cell.pricelbl.text = "kr " + pricee
            cell.cashLbl.text = orderDict.value(forKey: "payment_method") as! String
            
            let id = orderDict.value(forKey: "id") as! String
            let created_at = orderDict.value(forKey: "created_at") as! String
//2020-08-14 12:45:36
            // string to date
            
           let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let date = dateFormatter.date(from:created_at)!
            print("date",date)
            
            // date to string
                       
             let dateFormatter2 = DateFormatter()
              dateFormatter2.dateFormat = "hh:mm a dd MMM,yyyy"
//            let date2 = dateFormatter2.date(from:date)!
//            print("date2",date2)

            let dateShw = dateFormatter2.string(from: date)
            print("dateShw",dateShw)

            
            let orderdata = dict.value(forKey: "orderdata") as! NSDictionary

            if pastOrdersLbl.textColor == .white
            {
                let is_view = orderdata.value(forKey: "is_view") as! String
            if is_view == "1"
            {
            cell.viewOrderBtn.isHidden = false
            }
            else
            {
            cell.viewOrderBtn.isHidden = true
            }
            }
            
           let ordid = orderdata.value(forKey: "orderId") as! String
            cell.orderLbl.text = "Order #" + ordid
            cell.orderonLbl.text = "Order On " + dateShw

            return cell
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
            let dict = self.OrdersArr[indexPath.row] as! NSDictionary

            if pastOrdersLbl.textColor == .white
                     {
                         let orderdata = dict.value(forKey: "orderdata") as! NSDictionary
                         let is_view = orderdata.value(forKey: "is_view") as! String
                     if is_view == "1"
                     {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OrdersDetailVC") as! OrdersDetailVC
                              let dict = self.OrdersArr[indexPath.row] as! NSDictionary
                              vc.dict = dict
                              if newOrdersLbl.textColor == .white
                                           {
                                           vc.action = "new"
                                           }
                                           else if ongoingOrdersLbl.textColor == .white
                                           {
                                          vc.action = "current"
                                           }
                                           else
                                           {
                                               vc.action = "past"
                                           }
                              
                              
                      self.navigationController?.pushViewController(vc, animated: true)
                     }
                     else
                     {
                     }
                     }
            
            else
            {
            
     let vc = self.storyboard?.instantiateViewController(withIdentifier: "OrdersDetailVC") as! OrdersDetailVC
            let dict = self.OrdersArr[indexPath.row] as! NSDictionary
            vc.dict = dict
            if newOrdersLbl.textColor == .white
                         {
                         vc.action = "new"
                         }
                         else if ongoingOrdersLbl.textColor == .white
                         {
                        vc.action = "current"
                         }
                         else
                         {
                             vc.action = "past"
                         }
            
            
    self.navigationController?.pushViewController(vc, animated: true)
                
            }
            
            
    }
    // MARK: - locationupdate

    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
          let locValue:CLLocationCoordinate2D = manager.location!.coordinate
          print("locations = \(locValue.latitude) \(locValue.longitude)")
          self.topassLat = "\(locValue.latitude)"
          self.topassLong = "\(locValue.longitude)"
        
        let userLocation:CLLocation = locations[0] as CLLocation

        getAdressName(coords: userLocation)

        
       
      
        //  locationManager.stopUpdatingLocation()
          
      }

    func getAdressName(coords: CLLocation) {
        
        CLGeocoder().reverseGeocodeLocation(coords) { (placemark, error) in
            if error != nil {
                print("Hay un error")
            } else {
                
                let place = placemark! as [CLPlacemark]
                if place.count > 0 {
                    let place = placemark![0]
                    var adressString : String = ""
                    
                    if place.name != nil {
                        adressString = adressString + place.name! + ", "
                    }
                    
                    if place.thoroughfare != nil {
                        adressString = adressString + place.thoroughfare! + ", "
                    }
                    if place.subThoroughfare != nil {
                        adressString = adressString + place.subThoroughfare! + "\n"
                    }
                    if place.locality != nil {
                        adressString = adressString + place.locality! + " - "
                    }
                    if place.postalCode != nil {
                        adressString = adressString + place.postalCode! + "\n"
                    }
                    if place.subAdministrativeArea != nil {
                        adressString = adressString + place.subAdministrativeArea! + " - "
                    }
                    if place.country != nil {
                        adressString = adressString + place.country!
                    }
                    
                    self.addressStr = adressString
                    if self.OnlineLbl.text == "Online"
                         {
                            self.updateDriverLocation()
                         }
                         else
                         {
                             
                             
                         }
                }
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
