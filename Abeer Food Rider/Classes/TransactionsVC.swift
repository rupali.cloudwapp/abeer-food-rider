//
//  TransactionsVC.swift
//  Abeer Food Rider
//
//  Created by USER on 10/10/20.
//  Copyright © 2020 Cloudwapp. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Toast_Swift

class TransactionsVC: UIViewController,UITableViewDelegate,UITableViewDataSource {


// MARK: - Ibdecs
    
    var transactionsArr = NSArray()
    
    
 // MARK: - Iboutlets
    
    
    @IBOutlet weak var tableView: UITableView!
    
    
// MARK: - Ibactions
    
    
    
    
    @IBAction func backACT(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        TransactionsList()
    }
    func TransactionsList()  {
                 let type = NVActivityIndicatorType.ballScaleRippleMultiple
                 let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                 let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColor, padding: 20)
                 self.view.addSubview(activityIndicatorView)
                 self.view.isUserInteractionEnabled = false

                 activityIndicatorView.startAnimating()
                 var param = [String: Any]()
             let userData = UserDefaultManager().getUserDefault(key: "userData") as? NSDictionary
             
             param["driver_id"] = userData?.value(forKey: "id") as! String

                 WebService().postRequest(methodName: transaction, parameter: param) { (result) in
         activityIndicatorView.stopAnimating()
                     self.view.isUserInteractionEnabled = true

                     if let resultDict = result as? NSDictionary {
                         
                         print("resultDict",resultDict)
                         if resultDict.value(forKey: "status") as! Bool == true {
                             
                             if let userData = resultDict.value(forKey: "data") as? NSArray {
                                                        
                            
                                self.transactionsArr = userData
                                self.tableView.delegate = self
                                self.tableView.dataSource = self
                                self.tableView.reloadData()
                                
                             
                             }
                             }
                         else
                         {
                        self.view.makeToast("Sorry,Not able to show data.")
                         }
                     }
                 }
         }
    
    
    // MARK: - Tableview Methods:
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return self.transactionsArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TransactionsCell
        
        cell.selectionStyle = .none
        let dict = self.transactionsArr[indexPath.row] as! NSDictionary
        
        let price = dict.value(forKey: "amount") as! String
        cell.priceLbl.text = "kr " + price
        
        cell.statusLbl.text = dict.value(forKey: "txn_status") as! String
        if dict.value(forKey: "txn_status") as! String == "Pending"
        {}
        else
        {
            cell.statusLbl.textColor = themeColor
        }

       return cell
        
    }
    
      

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
