//
//  UserDefaultManager.swift
//  KCBerry
//
//  Created by Cloudwapp on 09/01/18.
//  Copyright © 2018 Cloudwapp Technologies. All rights reserved.
//

import UIKit

class UserDefaultManager: NSObject {
    let userDefaults = UserDefaults.standard

    func setUserDefault(value:AnyObject,key:String) {
        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: value)
        userDefaults.set(encodedData, forKey: key)
        userDefaults.synchronize()
    }
    
    func getUserDefault(key:String) -> AnyObject {
        if userDefaults.object(forKey: key) == nil {
            return "" as AnyObject
        }
        let decoded  = userDefaults.object(forKey: key) as! Data
        let decodedData = NSKeyedUnarchiver.unarchiveObject(with: decoded)
        return decodedData as AnyObject
    }
    
    func addFavourite(data:NSDictionary) {
        var array = NSMutableArray()
        var dataAlreadySave = false
        
        if getUserDefault(key: "favArray").isKind(of: NSArray.self)
        {
            array = (getUserDefault(key: "favArray") as! NSArray).mutableCopy() as! NSMutableArray
        }
        for dict in array {
            let idNum = ((dict as! NSDictionary).value(forKey: "product") as!NSDictionary).value(forKey: "ID") as! NSNumber
            if idNum == (data.value(forKey: "product") as!NSDictionary).value(forKey: "ID") as! NSNumber{
                dataAlreadySave = true
                var qty = (dict as! NSDictionary).value(forKey: "qty") as! Int
                if qty >= 100 {
                  //  Toast(text: "More than 10 is not allow.", delay: 0, duration: 0.3).show()
                    return
                }
                else{
                    qty = qty + 1
                    (dict as! NSDictionary).setValue(qty, forKey: "qty")
                    array.remove((dict as! NSDictionary))
                    array.add((dict as! NSDictionary))
                    setUserDefault(value: array, key: "favArray")
                   // Toast(text: "Item Quantity Add In Cart List!!!", delay: 0, duration: 0.3).show()
                }
            }
        }
        
        if dataAlreadySave == false {
            var dataDict = NSMutableDictionary()
            dataDict = data.mutableCopy() as! NSMutableDictionary
            dataDict.setValue(1, forKey: "qty")
            array.add(dataDict)
            setUserDefault(value: array, key: "favArray")
         //   Toast(text: "Item Add In Cart List!!!").show()
        }
    }
    
    func getFavourite() -> NSMutableArray {
        
        if getUserDefault(key: "favArray").isKind(of: NSArray.self)
        {
            let array = (getUserDefault(key: "favArray") as! NSArray).mutableCopy() as! NSMutableArray
            return array
        }
        return NSMutableArray()
    }
    
    func updateFavourite(data:NSDictionary,quantity:Int) -> NSMutableArray {
        var array = NSMutableArray()
        
        if getUserDefault(key: "favArray").isKind(of: NSArray.self)
        {
            array = (getUserDefault(key: "favArray") as! NSArray).mutableCopy() as! NSMutableArray
        }
        
        for dict in array {
           
            
            let idNum = ((dict as! NSDictionary).value(forKey: "product") as!NSDictionary).value(forKey: "ID") as! NSNumber
            if idNum == (data.value(forKey: "product") as!NSDictionary).value(forKey: "ID") as! NSNumber{
                let itemid = array.index(of: dict)
                
                var qty = (dict as! NSDictionary).value(forKey: "qty") as! Int
                qty = quantity
                (dict as! NSDictionary).setValue(qty, forKey: "qty")
                
                array.remove((dict as! NSDictionary))
                array.insert((dict as! NSDictionary), at: itemid)

                setUserDefault(value: array, key: "favArray")
                //Toast(text: "Item Quantity Add In Cart List!!!").show()
            }
        }
        
        return (getUserDefault(key: "favArray") as! NSArray).mutableCopy() as! NSMutableArray
    }
    
    
    
    func removeFavourite(data:NSDictionary) {
        var array = NSMutableArray()
        if getUserDefault(key: "favArray").isKind(of: NSArray.self)
        {
            array = (getUserDefault(key: "favArray") as! NSArray).mutableCopy() as! NSMutableArray
        }
        for index in 0..<array.count {
            let idNum = ((array.object(at: index) as! NSDictionary).value(forKey: "product") as!NSDictionary).value(forKey: "ID") as! NSNumber
            if idNum == (data.value(forKey: "product") as!NSDictionary).value(forKey: "ID") as! NSNumber{
                array.removeObject(at: index)
                break
            }
        }
        setUserDefault(value: array, key: "favArray")
      //  Toast(text: "Item Remove From Cart List!!!", delay: 0, duration: 0.3).show()
    }
    
    func removeAllFavourite()  {
        var array = NSMutableArray()
        if getUserDefault(key: "favArray").isKind(of: NSArray.self)
        {
            array = (getUserDefault(key: "favArray") as! NSArray).mutableCopy() as! NSMutableArray
            array.removeAllObjects()
        }
        setUserDefault(value: array, key: "favArray")
    }
    
}
