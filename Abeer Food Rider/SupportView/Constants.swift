//
//  Constants.swift
//  Teammates.net
//
//  Created by CP-02 on 05/12/18.
//  Copyright © 2018 CP-02. All rights reserved.
//

import Foundation
import AVKit


class StaticData{
    
    static let singleton = StaticData()
    init(){
        
        print("foo")
        
    }
    //for chat:
    
    var receiver_id:String? = ""
    var receiver_name:String? = ""
    var matchORinbox:String! = ""
    var receiver_img:String? = ""
    var isSticker:String? = ""
    var UrlImage:String? = ""
    
}

//let google_APIkey = "AIzaSyDAN3bYk_WhiDvW8pHNjlvXsPiwQo-YIFg"

//let google_APIkey = "AIzaSyDpLGc9pTTxvx4qAraD9bPV7oDgfBIpnj8"


let google_APIkey = "AIzaSyBaVuze86aAgEbbOLk_2tP2wpiG0dfqg2E"



//let google_APIkey = "AIzaSyDpLGc9pTTxvx4qAraD9bPV7oDgfBIpnj8"

//let base_url = "http://mobidudes.com/oohhsito/api/"
//let img_Url = "http://gofa.me/"
//let base_url = "https://oohhsito.com.pe/"


//let base_url = "http://gofa.me/api/"
//let base_url = "http://mobidudes.com/abeer/api/"
//let img_Url = "http://mobidudes.com/abeer/"

//http://gofa.me/


let base_url = "https://abeer.se/abeerapp/api/"
let img_Url = "https://abeer.se/abeerapp/"




let register = "Driver/register"
let forgot = "Forgot/forgot_password"

let login = "Driver/login"
let profileUpdate = "User/profile_update"
let locationUpdate = "Driver/location_update"
let onlineOffline = "Driver/online_offline"
let myorders = "Driver/myorders"
let deliverystart = "Driver/deliverystart"
let deliveryend = "Driver/deliveryfinished"
let trip_history = "Driver/trip_history"
let terms = "User/terms"

let showprofile = "Driver/showprofile"
let transaction = "Driver/transaction"
let cashout = "Driver/cashout"
let logout = "Driver/logout"
let change_password = "Driver/change_password"
let filterbydate = "Driver/filterbydate"
let accept = "Driver/accept"
let location_update = "Driver/location_update"
let contactdetail = "User/contactdetail"






//let menuGet = "User/menuget"


@available(iOS 13.0, *)
@available(iOS 14.0, *)



let appDele = UIApplication.shared.delegate as! AppDelegate


//let themeColor = UIColor(red: 255/255.0, green: 0/255.0, blue: 29/255.0, alpha: 1)
let themeColor = hexStringToUIColor(hex: "#008081")
let themeColorBiscay = hexStringToUIColor(hex: "#2D3E50")

let themeColorBrown  = hexStringToUIColor(hex: "#2D3E50")

//let themeColorYellow = UIColor(red: 254/255.0, green: 218/255.0, blue: 0/255.0, alpha: 1)

let themeColorYellow = hexStringToUIColor(hex: "#009DCB")

let themeColorBlue = UIColor(red: 30/255.0, green: 119/255.0, blue: 255/255.0, alpha: 1)
let themeColorRed = UIColor(red: 30/255.0, green: 119/255.0, blue: 255/255.0, alpha: 1)


//let themeColorBlue = UIColor(red: 30/255.0, green: 119/255.0, blue: 255/255.0, alpha: 1)
//let themeColorRed = UIColor(red: 254/255.0, green: 82/255.0, blue: 29/255.0, alpha: 1)

func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}
