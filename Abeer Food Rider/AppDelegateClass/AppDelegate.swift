//
//  AppDelegate.swift
//  Gofa Rider
//
//  Created by USER on 02/10/20.
//  Copyright © 2020 Cloudwapp. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase
import NotificationCenter
import FirebaseMessaging


@available(iOS 13.0, *)
@available(iOS 13.0, *)
@available(iOS 14.0, *)
@available(iOS 14.0, *)
@UIApplicationMain
class AppDelegate: UIResponder,UIApplicationDelegate,UNUserNotificationCenterDelegate,MessagingDelegate{

    override init() {
          super.init()
          UIView.classInit
      }
    
    var timer : Timer?

    var counter = 0
    var window: UIWindow?
       var fcmToken = String()
       var userDict = NSDictionary()
    var userName = String()
    var userId = String()
    var orderidStr = String()
    
    var notiAppear = false

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.shared.enable = true
    // UserDefaults.standard.set(false, forKey: "notiAppear")

       FirebaseApp.configure()
        
        if #available(iOS 10.0, *) {
                   // For iOS 10 display notification (sent via APNS)
                   UNUserNotificationCenter.current().delegate = self
                   
                   let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
                   UNUserNotificationCenter.current().requestAuthorization(
                       options: authOptions,
                       completionHandler: {_, _ in })
                   
               } else {
                   let settings: UIUserNotificationSettings =
                       UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
                   application.registerUserNotificationSettings(settings)
               }
               
               
               application.registerForRemoteNotifications()
               Messaging.messaging().delegate = self
               Messaging.messaging().isAutoInitEnabled = true
        
        return true
    }
    
    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil) -> Bool {
        UINavigationBar.appearance().barTintColor = UIColor(red:0.08, green:0.23, blue:0.62, alpha:1.0)

        if #available(iOS 13.0, *) {
            // In iOS 13 setup is done in SceneDelegate
        } else {
           
            if let newDict = UserDefaultManager().getUserDefault(key: "userData") as? NSDictionary {

                   
                        userDict = newDict.mutableCopy() as! NSDictionary
                        print("userDict",userDict)
                        userId = userDict.value(forKey: "id") as! String
                        userName = userDict.value(forKey: "fname") as! String
               self.setTabRootController()

               /* if UserDefaults.standard.bool(forKey: "notiAppear") == false
                {
                        self.setTabRootController()
                }
               else
                {
                 
                if let rootViewController = self.window!.rootViewController as? UINavigationController {
                                                                                                       
                                                                                                       let viewControllers = rootViewController.viewControllers
                                                                                                       for viewController in viewControllers {
                                                                                                           // some process
                                                                                                           if viewController.isKind(of: TabBarVC.self) {
                                                                                                               if let prestView = ((viewController as! TabBarVC).children[0] as! UINavigationController).visibleViewController as? OrdersVC {
                                                                                                               //prestView.orderidStr = newMsgDict["order_id"] as! String
                                                                                         //  prestView.counter = 0;
                                                                                                                 
                                                                                                                prestView.orderidStr =  self.orderidStr;                                prestView.ShowAcceptpopup()
                                                                                                            
                                                                                                               }
                                                                                                           }
                                                                                                       }
                                                                                                   }
                    
                    
                    
                }*/
                
                
                
                    }
            else
              {
                setLoginrPage()
            }
                    
        }

        return true
    }
    
    // MARK: for setting roots:

    
    func setTabRootController()  {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let rootController = storyboard.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
        
        let navigationView = UINavigationController(rootViewController: rootController)
        navigationView.navigationBar.isHidden = true
        
        if let window = self.window {
            window.rootViewController = navigationView
        }
    }
    
    func setLoginrPage()  {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let rootController = storyboard.instantiateViewController(withIdentifier: "loginVC") as! loginVC
        
        let navigationView = UINavigationController(rootViewController: rootController)
        navigationView.navigationBar.isHidden = true
        
        if let window = self.window {
            window.rootViewController = navigationView
        }
    }
    
    
    

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    //MARK: AppNotification Method
       func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
           
           print("deviceToken.description",deviceToken.description)
           
           Auth.auth().setAPNSToken(deviceToken, type: .prod)
           Messaging.messaging().apnsToken = deviceToken
       }
       
       func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
           if Auth.auth().canHandle(url) {
               return true
           }
           return true
       }
       func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
                InstanceID.instanceID().instanceID { (result, error) in
                    if let error = error {
                        print("Error fetching remote instance ID: \(error)")
                    } else if let result = result {
                        print("Remote instance ID token: \(result.token)")
                        self.fcmToken = fcmToken
                        print("self.fcmTokenUser", self.fcmToken)
                      
                      UserDefaults.standard.set(self.fcmToken, forKey: "self.fcmToken")
                        
                    }
                }
        }
    func stopTimer()
          {
            if timer != nil {
              timer!.invalidate()
              timer = nil
            }
          }
       @objc func prozessTimer() {
           counter += 1
           print("This is a second appdele", counter)

       }
       
       
    func application(_ application: UIApplication,
                                        didReceiveRemoteNotification notification: [AnyHashable : Any],
                                        fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
            
                   
                   if Auth.auth().canHandleNotification(notification) {
                                   completionHandler(.noData)
                                  // return
                               }
        
                   
                   print("just received")
        
      //  let type  = notification.request.content.userInfo[AnyHashable("gcm.notification.type")] as? String
                          
                          
        let userInfo = notification
        
      
       if let msgData = userInfo["payload"] as? String {
                let msgDict = msgData.stringToJson()
                if let newMsgDict = msgDict as? NSDictionary {
                    
                    
                    print("payloadnewMsgDict",newMsgDict)
                    
                    if let type = newMsgDict["type"] as? String
                    {
                        
                      
                     //type-managemnet
                        let typestr = type
                        if type == "order"
                        {
                            
                            self.orderidStr = newMsgDict["order_id"] as! String
                            
                    UserDefaults.standard.set(self.orderidStr, forKey: "self.orderidStr")
                            
                            counter = 0
                            UserDefaults.standard.set(true, forKey: "notiAppear")

                            if timer == nil {
                                 
                             timer = Timer.scheduledTimer(timeInterval:1, target:self, selector:#selector(prozessTimer), userInfo: nil, repeats: true)
                            }
                           

                            if counter >= 60 * 5
                             {
                                notiAppear = false
                                 stopTimer()
                                // counter = 0
                                
                            UserDefaults.standard.set(false, forKey: "notiAppear")
                            
                             }
                            else
                             {
                                notiAppear = true
                                UserDefaults.standard.set(true, forKey: "notiAppear")

                            }
                         
                            
                            UserDefaults.standard.set(counter, forKey: "counterStr")
                            
                           if #available(iOS 13.0, *)  {
                                                                                                                                                
                                                                                     let scene = UIApplication.shared.connectedScenes.first
                                                                                if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                                                                                   
                                                       if let rootViewController = sd.window!.rootViewController as? UINavigationController {
                                                                                                            
                                                                                                            let viewControllers = rootViewController.viewControllers
                                                                                                            for viewController in viewControllers {
                                                                                                                // some process
                                                                                                                if viewController.isKind(of: TabBarVC.self) {
                                                                                                                    if let prestView = ((viewController as! TabBarVC).children[0] as! UINavigationController).visibleViewController as? OrdersVC {
                                                                                                                    prestView.orderidStr = newMsgDict["order_id"] as! String
                                                                                                                        self.orderidStr = newMsgDict["order_id"] as! String
                                                                                                                        
                                                                                                                        //UserDefaults.standard.set(self.orderidStr, forKey: "self.orderidStr")
                                                                                                                        
                                                                       prestView.counter = counter;                                               prestView.ShowAcceptpopup()
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                   
                                                                                }
                                                                           }
                            if #available(iOS 14.0, *)  {
                                                                                                                                                                           
                                                                                                                let scene = UIApplication.shared.connectedScenes.first
                                                                                                           if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                                                                                                              
                                                                                  if let rootViewController = sd.window!.rootViewController as? UINavigationController {
                                                                                                                                       
                                                                                                                                       let viewControllers = rootViewController.viewControllers
                                                                                                                                       for viewController in viewControllers {
                                                                                                                                           // some process
                                                                                                                                           if viewController.isKind(of: TabBarVC.self) {
                                                                                                                                               if let prestView = ((viewController as! TabBarVC).children[0] as! UINavigationController).visibleViewController as? OrdersVC {
                                                                                                                                               prestView.orderidStr = newMsgDict["order_id"] as! String
                                                                                                                                                   self.orderidStr = newMsgDict["order_id"] as! String
                                                                                                                                                   
                                                                                                                                                   //UserDefaults.standard.set(self.orderidStr, forKey: "self.orderidStr")
                                                                                                                                                   
                                                                    prestView.counter = counter;                                               prestView.ShowAcceptpopup()
                                                                                                                                               }
                                                                                                                                           }
                                                                                                                                       }
                                                                                                                                   }
                                                                                                              
                                                                                                           }
                                                                                                      }
                                                                           else  {
                                                  if let rootViewController = self.window!.rootViewController as? UINavigationController {
                                                                                      
                                                                                      let viewControllers = rootViewController.viewControllers
                                                                                      for viewController in viewControllers {
                                                                                          // some process
                                                                                          if viewController.isKind(of: TabBarVC.self) {
                                                                                              if let prestView = ((viewController as! TabBarVC).children[0] as! UINavigationController).visibleViewController as? OrdersVC {
                                                                                              prestView.orderidStr = newMsgDict["order_id"] as! String
                                                                         prestView.counter = counter;
                                                                                                
                                                                    self.orderidStr = newMsgDict["order_id"] as! String
                                                                                                //UserDefaults.standard.set(self.orderidStr, forKey: "self.orderidStr")
                                                                                                
                                                                                                prestView.ShowAcceptpopup()
                                                                                           
                                                                                              }
                                                                                          }
                                                                                      }
                                                                                  }
                            
                                                                               }
                        }
                    else  if type == "cancelorder" || type == "cancel"
                                               {
                                               
                                                  if #available(iOS 13.0, *)  {
                                                                                                                                                                       
                                                                                                            let scene = UIApplication.shared.connectedScenes.first
                                                                                                       if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                                                                                                          
                                                                              if let rootViewController = sd.window!.rootViewController as? UINavigationController {
                                                                                                                                   
                                                                                                                                   let viewControllers = rootViewController.viewControllers
                                                                                                                                   for viewController in viewControllers {
                                                                                                                                       // some process
                                                                                                                                       if viewController.isKind(of: TabBarVC.self) {
                                                                                                                                           if let prestView = ((viewController as! TabBarVC).children[0] as! UINavigationController).visibleViewController as? OrdersVC {
                                                                                                                                           prestView.cancelOrderid = newMsgDict["order_id"] as! String
                                                                                                                                            prestView.cancelOrderStore = newMsgDict["store_name"] as! String;                                                                         prestView.ShowCancelOrderpopup()
                                                                                                                                           }
                                                                                                                                       }
                                                                                                                                   }
                                                                                                                               }
                                                                                                          
                                                                                                       }
                                                                                                  }
                                                    if #available(iOS 14.0, *)  {
                                                                                                                                             
                                                                                  let scene = UIApplication.shared.connectedScenes.first
                                                                             if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                                                                                
                                                    if let rootViewController = sd.window!.rootViewController as? UINavigationController {
                                                                                                         
                                                                                                         let viewControllers = rootViewController.viewControllers
                                                                                                         for viewController in viewControllers {
                                                                                                             // some process
                                                                                                             if viewController.isKind(of: TabBarVC.self) {
                                                                                                                 if let prestView = ((viewController as! TabBarVC).children[0] as! UINavigationController).visibleViewController as? OrdersVC {
                                                                                                                 prestView.cancelOrderid = newMsgDict["order_id"] as! String
                                                                                                                  prestView.cancelOrderStore = newMsgDict["store_name"] as! String;                                                                         prestView.ShowCancelOrderpopup()
                                                                                                                 }
                                                                                                             }
                                                                                                         }
                                                                                                     }
                                                                                
                                                                             }
                                                                        }
                                                                                                  else  {
                                                                         if let rootViewController = self.window!.rootViewController as? UINavigationController {
                                                                                                             
                                                                                                             let viewControllers = rootViewController.viewControllers
                                                                                                             for viewController in viewControllers {
                                                                                                                 // some process
                                                                                                                 if viewController.isKind(of: TabBarVC.self) {
                                                                                                                     if let prestView = ((viewController as! TabBarVC).children[0] as! UINavigationController).visibleViewController as? OrdersVC {
                                                                       prestView.cancelOrderid = newMsgDict["order_id"] as! String
                                                                                                                        prestView.cancelOrderStore = newMsgDict["store_name"] as! String;                                                 prestView.ShowCancelOrderpopup()
                                                                                                                     }
                                                                                                                 }
                                                                                                             }
                                                                                                         }
                                                   
                                                                                                      }
                                               }
                        
                    }
                        
                    else
                    {
                        if let rootViewController = self.window!.rootViewController as? UINavigationController {
                            
                            let viewControllers = rootViewController.viewControllers
                            for viewController in viewControllers {
                                // some process
                                if viewController.isKind(of: TabBarVC.self) {
                                    
                                    if let prestView = ((viewController as! TabBarVC).children[0] as! UINavigationController).visibleViewController as? OrdersVC {
                                        //  let prestView = ((viewController as! TabBarVC).children[0] as! UINavigationController).visibleViewController as? HomeVC
                                        print("removeAnimateDo2")
                                      //  prestView.counter = 0;
                                        prestView.toRefresh()
                                        
                                    }
                                    
                                    //return
                                }
                            }
                        }
                    }
                    
                    
                    
                    
                }
                    
                }
             
                   
                   print("userinfo",notification)
                   completionHandler(UIBackgroundFetchResult.newData)
               }
               
               // Firebase notification received
  func userNotificationCenter(_ center: UNUserNotificationCenter,  willPresent notification: UNNotification, withCompletionHandler   completionHandler: @escaping (_ options:   UNNotificationPresentationOptions) -> Void) {
                   completionHandler([.alert, .badge, .sound])
                   // custom code to handle push while app is in the foreground
                   print("Handle push from foreground, received: \n \(notification.request.content)")
                   print(notification.request.content.userInfo)
                   
                 //  let type  = notification.request.content.userInfo[AnyHashable("gcm.notification.type")] as? String
                   
                   
                   let userInfo = notification.request.content.userInfo
    
    
    
     /*  if let msgData = userInfo["payload"] as? String {
         let msgDict = msgData.stringToJson()
         if let newMsgDict = msgDict as? NSDictionary {
             
             if let type = newMsgDict["type"] as? String
             {
                 
                 let typestr = type
                 if type == "order"
                 {
                    
                    if #available(iOS 13.0, *) {
                                                                                                                                         
                                                                              let scene = UIApplication.shared.connectedScenes.first
                                                                         if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                                                                            
                                                if let rootViewController = sd.window!.rootViewController as? UINavigationController {
                                                                                                     
                                                                                                     let viewControllers = rootViewController.viewControllers
                                                                                                     for viewController in viewControllers {
                                                                                                         // some process
                                                                                                         if viewController.isKind(of: TabBarVC.self) {
                                                                                                             if let prestView = ((viewController as! TabBarVC).children[0] as! UINavigationController).visibleViewController as? OrdersVC {
                                                                                                             prestView.orderidStr = newMsgDict["order_id"] as! String
                                                                      //   prestView.counter = 0;
                                                                                                                prestView.ShowAcceptpopup()
                                                                                                          
                                                                                                             }
                                                                                                         }
                                                                                                     }
                                                                                                 }
                                                                            
                                                                         }
                                                                    }
                                                                    else  {
                                           if let rootViewController = self.window!.rootViewController as? UINavigationController {
                                                                               
                                                                               let viewControllers = rootViewController.viewControllers
                                                                               for viewController in viewControllers {
                                                                                   // some process
                                                                                   if viewController.isKind(of: TabBarVC.self) {
                                                                                       if let prestView = ((viewController as! TabBarVC).children[0] as! UINavigationController).visibleViewController as? OrdersVC {
                                                                                       prestView.orderidStr = newMsgDict["order_id"] as! String
                                                                                      //  prestView.counter = 0;
                                                                                        prestView.ShowAcceptpopup()
                                                                                    
                                                                                       }
                                                                                   }
                                                                               }
                                                                           }
                                                                        }
                    
                 }
                 
             }
             else
             {
                 
                 if let rootViewController = self.window!.rootViewController as? UINavigationController {
                     
                     let viewControllers = rootViewController.viewControllers
                     for viewController in viewControllers {
                         // some process
                         if viewController.isKind(of: TabBarVC.self) {
                             
                             if let prestView = ((viewController as! TabBarVC).children[0] as! UINavigationController).visibleViewController as? OrdersVC {
                                 //  let prestView = ((viewController as! TabBarVC).children[0] as! UINavigationController).visibleViewController as? HomeVC
                                 print("removeAnimateDo2")
                                 
                                 prestView.toRefresh()
                                 
                             }
                             
                             //return
                         }
                     }
                 }
                 
                 
             }
             
             
             
             
         }
             
         }*/
    
    
    
    
        
        if let rootViewController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
            
            let viewControllers = rootViewController.viewControllers
            for viewController in viewControllers {
                // some process
                if viewController.isKind(of: TabBarVC.self) {
                    
                    if let prestView = ((viewController as! TabBarVC).children[0] as! UINavigationController).visibleViewController as? OrdersVC {
                        //  let prestView = ((viewController as! TabBarVC).children[0] as! UINavigationController).visibleViewController as? HomeVC
                        print("removeAnimateDo2")
                        
                        prestView.toRefresh()
                        
                    }
                    
                    //return
                }
            }
        }
        
        
        
  //  }
        
    }
    
    
                
                   

           
               func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse,
                                           withCompletionHandler completionHandler: @escaping () -> Void) {
                   print("Handle tapped push from background, received: \n \(response.notification.request.content)")
                   
                 //  let type  = notification.request.content.userInfo[AnyHashable("gcm.notification.type")] as? String
                   
                   
                let userInfo = response.notification.request.content.userInfo

               /* if counter >= 60
                 {
                    
                     stopTimer()
                    // counter = 0

                 }
                else
                 {

                     if let msgData = userInfo["payload"] as? String {
                       let msgDict = msgData.stringToJson()
                       if let newMsgDict = msgDict as? NSDictionary {
                           
                           if let type = newMsgDict["type"] as? String
                           {
                               
                               let typestr = type
                               if type == "order"
                               {
                                  
                                  if #available(iOS 13.0, *) {
                                                                                                                                                       
                                                                                            let scene = UIApplication.shared.connectedScenes.first
                                                                                       if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                                                                                          
                                                              if let rootViewController = sd.window!.rootViewController as? UINavigationController {
                                                                                                                   
                                                                                                                   let viewControllers = rootViewController.viewControllers
                                                                                                                   for viewController in viewControllers {
                                                                                                                       // some process
                                                                                                                       if viewController.isKind(of: TabBarVC.self) {
                                                                                                                           if let prestView = ((viewController as! TabBarVC).children[0] as! UINavigationController).visibleViewController as? OrdersVC {
                                                                                                                           prestView.orderidStr = newMsgDict["order_id"] as! String
                                                                                     //  prestView.counter = 0;
                                                                                                                            prestView.ShowAcceptpopup()
                                                                                                                        
                                                                                                                           }
                                                                                                                       }
                                                                                                                   }
                                                                                                               }
                                                                                          
                                                                                       }
                                                                                  }
                                                                                  else  {
                                                         if let rootViewController = self.window!.rootViewController as? UINavigationController {
                                                                                             
                                                                                             let viewControllers = rootViewController.viewControllers
                                                                                             for viewController in viewControllers {
                                                                                                 // some process
                                                                                                 if viewController.isKind(of: TabBarVC.self) {
                                                                                                     if let prestView = ((viewController as! TabBarVC).children[0] as! UINavigationController).visibleViewController as? OrdersVC {
                                                                                                     prestView.orderidStr = newMsgDict["order_id"] as! String
                                                                                                      //prestView.counter = 0;
                                                                                                        prestView.ShowAcceptpopup()
                                                                                                  
                                                                                                     }
                                                                                                 }
                                                                                             }
                                                                                         }
                                                                                      }
                                     
                               }
                               
                           }
                           else
                           {
                               
                               if let rootViewController = self.window!.rootViewController as? UINavigationController {
                                   
                                   let viewControllers = rootViewController.viewControllers
                                   for viewController in viewControllers {
                                       // some process
                                       if viewController.isKind(of: TabBarVC.self) {
                                           
                                           if let prestView = ((viewController as! TabBarVC).children[0] as! UINavigationController).visibleViewController as? OrdersVC {
                                               //  let prestView = ((viewController as! TabBarVC).children[0] as! UINavigationController).visibleViewController as? HomeVC
                                               print("removeAnimateDo2")
                                               
                                               prestView.toRefresh()
                                               
                                           }
                                           
                                           //return
                                       }
                                   }
                               }
                               
                               
                           }
                           
                           
                           
                           
                       }
                           
                       }
                      
                      

                 }*/
                
                
                
               
                  
                   
                   
                   completionHandler()
               }
           
     
       //MARK: messaging Delegate Method
     
       
       func logoutFirebaseUser() {
           let firebaseAuth = Auth.auth()
           do {
               try firebaseAuth.signOut()
           } catch let signOutError as NSError {
               print ("Error signing out: %@", signOutError)
           }
       }
       
       func applicationWillResignActive(_ application: UIApplication) {
           // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
           // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
       }
       
       func applicationDidEnterBackground(_ application: UIApplication) {
           // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
           // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
       }
       
       func applicationWillEnterForeground(_ application: UIApplication) {
           // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
       }
       
       func applicationDidBecomeActive(_ application: UIApplication) {
           // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
       }
       
       func applicationWillTerminate(_ application: UIApplication) {
           // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
           // Saves changes in the application's managed object context before the application terminates.
       }
       

}

private let swizzling: (AnyClass, Selector, Selector) -> () = { forClass, originalSelector, swizzledSelector in
        guard
            let originalMethod = class_getInstanceMethod(forClass, originalSelector),
            let swizzledMethod = class_getInstanceMethod(forClass, swizzledSelector)
            else { return }
        method_exchangeImplementations(originalMethod, swizzledMethod)
    }

    
    extension UIView {
        
        static let classInit: Void = {
            let originalSelector = #selector(layoutSubviews)
            let swizzledSelector = #selector(swizzled_layoutSubviews)
            swizzling(UIView.self, originalSelector, swizzledSelector)
        }()
        
        @objc func swizzled_layoutSubviews() {
            swizzled_layoutSubviews()
          //  print("swizzled_layoutSubviews")
        }
        
    }

extension Data {
    var hexString: String {
        let hexString = map { String(format: "%02.2hhx", $0) }.joined()
        return hexString
    }
}


