//
//  ContentView.swift
//  Gofa Rider
//
//  Created by USER on 02/10/20.
//  Copyright © 2020 Cloudwapp. All rights reserved.
//

import SwiftUI

@available(iOS 13.0.0, *)
@available(iOS 13.0.0, *)
@available(iOS 13.0.0, *)
@available(iOS 13.0, *)

struct ContentView: View {
    @available(iOS 13.0.0, *)
    var body: some View {
        Text("Hello World")
    }
}

@available(iOS 13.0, *)

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

