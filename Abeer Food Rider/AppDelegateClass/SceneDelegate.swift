//
//  SceneDelegate.swift
//  Gofa Rider
//
//  Created by USER on 02/10/20.
//  Copyright © 2020 Cloudwapp. All rights reserved.
//

import UIKit
import SwiftUI

@available(iOS 13.0, *)
class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
       var fcmToken = String()
       var userDict = NSDictionary()
    var userName = String()
      var userId = String()

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
      
      if let newDict = UserDefaultManager().getUserDefault(key: "userData") as? NSDictionary {

                              userDict = newDict.mutableCopy() as! NSDictionary
                              print("userDict",userDict)
                              userId = userDict.value(forKey: "id") as! String
                              userName = userDict.value(forKey: "fname") as! String
                              self.setTabRootController()
                          }
        else
      {
        setLoginrPage()
        
        }
        
        UIApplication.shared.windows.forEach { window in
                        window.overrideUserInterfaceStyle = .light
                    }
        
    }
    
    // MARK: for setting roots:

      
      func setTabRootController()  {
          let storyboard = UIStoryboard(name: "Main", bundle: nil)
          let rootController = storyboard.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
          
          let navigationView = UINavigationController(rootViewController: rootController)
          navigationView.navigationBar.isHidden = true
          
          if let window = self.window {
              window.rootViewController = navigationView
          }
      }
      
      func setLoginrPage()  {
          let storyboard = UIStoryboard(name: "Main", bundle: nil)
          let rootController = storyboard.instantiateViewController(withIdentifier: "loginVC") as! loginVC
          
          let navigationView = UINavigationController(rootViewController: rootController)
          navigationView.navigationBar.isHidden = true
          
          if let window = self.window {
              window.rootViewController = navigationView
          }
      }
      

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }


}


