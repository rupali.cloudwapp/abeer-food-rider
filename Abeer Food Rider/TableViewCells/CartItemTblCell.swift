//
//  CartItemTblCell.swift
//  Oohhsito
//
//  Created by CW_3 on 19/07/19.
//  Copyright © 2019 CW. All rights reserved.
//

import UIKit

class CartItemTblCell: UITableViewCell {
   
   
        @IBOutlet weak var qty_lbl: UILabel!
        @IBOutlet weak var titleLbl: UILabel!
        @IBOutlet weak var priceLbl: UILabel!
        @IBOutlet weak var subBtn: DesignableButton!
        @IBOutlet weak var plusBtn: DesignableButton!
        @IBOutlet weak var countLbl: UILabel!
        @IBOutlet weak var productPriceLbl: UILabel!
        @IBOutlet weak var customizedBtn: UIButton!
        @IBOutlet weak var line_lbl: UILabel!
        @IBOutlet weak var drop_img: UIImageView!
        @IBOutlet weak var AdonTblView: UITableView!
        
        
        @IBOutlet weak var adonViewHeight: NSLayoutConstraint!
        
        @IBOutlet weak var adonTbHeight: NSLayoutConstraint!
        
        
        
        override func awakeFromNib() {
            super.awakeFromNib()
            // Initialization code
           // AdonTblView.separatorStyle = .none
            //AdonTblView.isScrollEnabled = false
        }

        override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)

            // Configure the view for the selected state
        }
        
        func setTblViewDataSourceDelegate(dataSourceDelegate: UITableViewDataSource & UITableViewDelegate, forRow row: Int) {
               AdonTblView.delegate = dataSourceDelegate
               AdonTblView.dataSource = dataSourceDelegate
               AdonTblView.tag = row
               AdonTblView.reloadData()
           }

    }

