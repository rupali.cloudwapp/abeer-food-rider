//
//  OrderCell.swift
//  Gofa Rider
//
//  Created by USER on 02/10/20.
//  Copyright © 2020 Cloudwapp. All rights reserved.
//

import UIKit

class OrderCell: UITableViewCell {
    
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var pricelbl: UILabel!
    @IBOutlet weak var cashLbl: UILabel!
    @IBOutlet weak var orderLbl: UILabel!
    @IBOutlet weak var orderonLbl: UILabel!
    @IBOutlet weak var viewOrderBtn: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
